<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">
  <?php $PAGE = 'Modules'; ?>
  <title> Modules | Portal page </title>

  <!-- Bootstrap CSS -->    
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />    
  <!-- Custom styles -->
  <link rel="stylesheet" href="css/fullcalendar.css">
  <link href="css/widgets.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />

  <link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">

  <link rel="stylesheet" href="css/bootstrap-dialog.min.css">

</head>

<body>
  <!-- container section start -->
  <section id="container" class="">



    <!--header end-->
    <?php  include 'header.php' ; ?>   
    <!--sidebar start-->
    <?php  include 'sidebar.php' ; ?>   
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">            
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">

           <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
            <li><i class="fa fa-laptop"></i><a href="modules.php">Program Modules</a></li>						  	
          </ol>
        </div>
      </div>

      <!-- Today status end -->              

      <div class="row">

 <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                            REGISTERED MODULES
                          </header>
                          
                          <table class="table table-striped table-advance table-hover">
                           <tbody>
                              <tr>                                
                                 <th> Module Code </th>
                                 <th> Module Name</th>
                                 <th> Posts</th>
                                 <th> Material </th>
                                 <th> Assignments</th>
                                  <th></i> Cswk</th>
                              </tr>

                             <?php
                              require 'dbconx.php';
                             $LEVEL = mysqli_fetch_assoc(mysqli_query($con , "SELECT * FROM users_students WHERE registration_number= '$_SESSION[user]' "))['level'];
                             $year = explode(".", $LEVEL)[0];
                             $semester =explode(".", $LEVEL)[1];
                             $sq  = "SELECT * FROM  programs WHERE gYear = '$year' AND sem = '$semester' ";
                             $qy = mysqli_query($con , $sq);
                            // while($dr = mysqli_fetch_assoc($qy)){                            
                             
                             for($i =0 ; $i<3 ; $i++){
                              $CModuleCode = 'CUIT404'.$i;?>
                             <tr id="trw" class="trw">
                                 <td class="CModuleCode" style="cursor:pointer; text-decoration: underline;"><?php echo $CModuleCode; ?></td>
                                 <td class="ModuleName" style="cursor:pointer;text-decoration: underline;">COMPUTER SECURITY</td>
                                 <td class="Cposts" style="cursor:pointer;text-decoration: underline;">(0) Posts</td>
                                 <td class="CReadingMaterial" style="cursor:pointer;text-decoration: underline;">31 Reading Material</td>
                                 <td  class="CAssignments" style="cursor:pointer;text-decoration: underline;">(12) Assignments</td>
                                  <td  class="Cswk" style="cursor:pointer;text-decoration: underline;" >Cswk</td>                                
                              </tr> 

                             <tr class="viewCswk" style="display: none;" >
                                <td colspan="6"> 
                                 <center>
                                   <label>
                                     <strong>Course Work : Marks of this part ares </strong>
                                   </label>
                                 </center>
                                  
                                </td>
                                                               
                             </tr>
                             <tr class="viewAssignments" style="display: none;" >
                                <td colspan="6"> 
                                 
                                
                                   <div class="row" style="padding: 0 20px;margin: 20px 10px;">
                                     <b><label>Upload Asssigment</label></b>
                                     <br>
                                     <input type="file" name="<?php echo $CModuleCode;  ?>" id="fileAssignment" demin="<?php echo $i; ?>">
                                     <br>
                                     <button class="btn btn-primary uploadAssignment" id="uploadAssignment" deminVal ="<?php echo $i; ?>"CModuleCode="<?php echo $CModuleCode ; ?>"   >Send File</button>
                                   </div>
                                
                                  
                                </td>
                                                               
                             </tr>
                             <tr class="viewCReadingMaterial" style="display: none;" >
                                <td colspan="6"> 
                                 <table class="table table-striped table-advance table-hover">
                                   <thead class="mdb-color lighten-4">
                                    <tr>
                                        <th>&nbsp;</th>
                                      <th>File Name </th>  
                                      <th>&nbsp;</th>
                                      <th>File Size</th>
                                      <th>&nbsp;</th>
                                      <th>Download File</th>
                                      </tr>

                                   </thead>

                                    <tbody>
                                      
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td id="downFileName" >OutLine</td>
                                        <td>&nbsp;</td>
                                        <td id="downFileSize">23 MB</td>
                                        <td>&nbsp;</td>
                                        <td><a class="donwloadFileModuleFile" CModuleCode="<?php echo $CModuleCode ; ?>"  onclick="return false;" href="34582.pdf">Download</a></td>
                                      </tr>

                                    </tbody>
                                  </table>
                                  
                                </td>
                                                               
                             </tr>

                             <?php } //  } ?>
                            

                           
                                                         
                           </tbody>
                        </table>
                      </section>
                  </div>
              </div>



      </div>



      <!-- statics end -->





    </section>

  </section>
  <!--main content end-->
</section>
<!-- container section start -->

<!-- javascripts -->
<script src="js/jquery.js"></script>
<script src="js/jquery-ui-1.10.4.min.js"></script>
<script src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<!-- bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.nicescroll.js" type="text/javascript"></script>   
<!--custome script for all page-->
<script src="js/scripts.js"></script>
<!-- custom script for this page-->

<script src="js/jquery.autosize.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/gdp-data.js"></script>	
<script src="js/morris.min.js"></script>

<script src="js/jquery.slimscroll.min.js"></script>
<!-- cutsome use -->
<script type="text/javascript" src="js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" src="js/loadingoverlay.min.js"></script>
<script type="text/javascript" src="js/loadingoverlay_progress.min.js"></script>
<script src="js/customeQuery.js"></script> 
<script type="text/javascript">
  $(function() {
  pagevisitStudent();

});
</script>

</body>
</html>
