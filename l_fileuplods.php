<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">
  <?php $PAGE = 'Lectures File uploads'; ?>
  <title> Admin  page </title>

  <!-- Bootstrap CSS -->    
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />    
  <!-- Custom styles -->
  <link rel="stylesheet" href="css/fullcalendar.css">
  <link href="css/widgets.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />
  
  <link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">

  <link rel="stylesheet" href="css/bootstrap-dialog.min.css">

</head>


<body>
  <!-- container section start -->
  <section id="container" class="">



    <!--header end-->
    <?php  include 'header.php' ; ?>   
    <!--sidebar start-->
    <?php  include 'sidebar.php' ; ?>   
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">            
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">

            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="admin_index.php">Home</a></li>
              <li><i class="fa fa-table"></i><a href="l_fileuplods.php">Lecturer File Upload </a></li>

            </ol>
          </div>
        </div>

        <!-- Today status end -->              
        
        <div class="row">
           <!-- start of graph -->
<div class="col-lg-12" id="showgraphsDaillyReports" style="display: none;">
                      <section class="panel">
                          <header class="panel-heading">
                           Showing Graph :
                          </header>
                          <div class="panel-body">
                             
                              <form class="form-horizontal "  onsubmit="return false;">
                                <div class="form-group"  style="" >
                                      
                                      <div class="col-lg-10">
                                          <button onClick="backDownloadreport()" class="btn btn-primary" >Back </button>

                                      </div>
                                  </div>
                                   <div style="" class="row">
                                    <div class="col-sm-2">
          <button onClick="showHighfileUploadLecturerCharts()" class="btn btn-success" >Bar Graph </button>
        </div>
        <div class="col-sm-2">
          <button onClick="downloadsfileUploadLecturerPieChart_btter()" class="btn btn-success" >Pie Chart </button>
        </div>
         <div class="col-sm-2">
          <button onClick="downloadsfileUploadLecturerDonutChart()" class="btn btn-success" >Donought Chart </button>
        </div>
              
                                   </div>
                                  <div class="form-group">
                                    <div id="container11" style="margin: 60px 0;">
                                      
                                    </div>
                                     <div id="containerPie" style="margin: 60px 0;">
                                      
                                    </div>
                                    
                                  </div>
                                  
                              </form>
                            
                          </div>
                           
                      </section>
                     
                     
                     
                  </div>

<!-- end of graph -->

          <div class="col-lg-12" id="graphDaillyreportFunctions">
            <section class="panel" style="">
              <header class="panel-heading">
               Filter/Search by :
             </header>
             <div class="panel-body">

              <form class="form-horizontal"   onsubmit="return false;">
                <div class="form-group"  style="" >
                  <label class="control-label col-lg-2" for="inputSuccess">  </label>
                  <div class="col-lg-10">

                   <div class="col-lg-6" >
                    <label>  <label>
                      <input style="width: 430px;" id="lectureFileListsFilter"  type="text" class="form-control" placeholder="Module Code , RegNumber">
                    </div>
                    <div  class="col-lg-2" >
                      <input type="submit" class="btn btn-success" onclick="btnlectureFileLists()"  value="Find">
                    </div>
                    <div  class="col-lg-2" >
                      <input type="reset" class="btn" onclick="btnlectureFileListsReset()"  value="Reset">
                    </div>

                  </div>
                </div>

              </form>
              <div class="form-group" id="actionDailyReports" style=";">
                <label  class="control-label col-lg-2" for="inputSuccess">Action</label>
                <div class="col-lg-10">
                  <label class="checkbox-inline">
                    <input type="submit" style="display: none;" onClick ="excelfyDownload()" class="btn btn-success" id="inlineCheckbox1" value="Export to Excel"> 
                  </label>
                  <label class="checkbox-inline" style="display: none;">
                    <input type="submit" id="" onClick ="csvfyDownload_lectureFile()" class="btn btn-success" value="Export to CSV File"> 
                 </label>
                 <label class="checkbox-inline">
                  <input type="submit" id="" onClick="printDailyReport()" class="btn btn-success" value="Print Table"> 
                </label>
                <label class="checkbox-inline">
                  <input type="submit" id="" onClick="showReportGraphs()" class="btn btn-success" value="View Graph"> 
                </label>

              </div>

            </div>

            </div>


          </section>



        </div>


      </div>
        <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Report on File uploads done by Lectures
                          </header>
                          <div class="table-responsive">
                            <table id="tableID" class="table table-bordered">
                              <thead>
                                <tr>
                                   <th></th> 
                                  <th>Module Code</th>
                                  <th>Lecturer </th>
                                  <th>File uploaded Counts</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody id="show_fileupload">
                                <?php 
                                require 'dbconx.php';
                                // SELECT NUM, count(*) as 'Cnt' FROM Table1 GROUP BY NUM
                                // SELECT `module_code`, count(*) as 'count' ,`lecturer` FROM reading_materials GROUP BY `module_code`
                                // CUACE101   c32123123232x

                                $sql  = mysqli_query($con , "SELECT `module_code`, count(*) as 'count' ,`lecturer` FROM reading_materials GROUP BY `module_code` ");
                                while($we = mysqli_fetch_assoc($sql)){

                                  ?>                                
                                <tr>
                                 <th></th>                                  
                                  <td >   <?php   echo $we['module_code'] ;   ?>   </td>
                                  <td >   <?php   echo $we['lecturer'] ;  ?>   </td>                              
                                   <td >   <?php   echo $we['count'] ;  ?>   </td>  
                                   <th></th>                                                       
                                  
                                </tr>
                                <?php  }  ?>
                               </tbody>
                            </table>
                          </div>

                      </section>
                  </div>
              </div>



      <!-- statics end -->





    </section>

  </section>
  <!--main content end-->
</section>
<!-- container section start -->

<!-- javascripts -->
<script src="js/jquery.js"></script>
<script src="js/jquery-ui-1.10.4.min.js"></script>
<script src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<!-- bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.nicescroll.js" type="text/javascript"></script>   
<!--custome script for all page-->
<script src="js/scripts.js"></script>
<!-- custom script for this page-->

<script src="js/jquery.autosize.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/gdp-data.js"></script>  
<script src="js/morris.min.js"></script>

<script src="js/jquery.slimscroll.min.js"></script>
<!-- cutsome use -->
<script type="text/javascript" src="js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" src="js/loadingoverlay.min.js"></script>
<script type="text/javascript" src="js/loadingoverlay_progress.min.js"></script>

 <script src="js/highcharts.js"></script>
<script src="js/series-label.js"></script>
<script src="js/exporting.js"></script>


<script src="js/customeQuery.js"></script> 
<script type="text/javascript">
  function showReportGraphs(){ 
  
  $("#graphDaillyreportFunctions").slideUp("slow", function() {

    $("#showgraphsDaillyReports").slideDown(400, function() {
     showGraphslecture_fileuploads();
    });
  }); 
}
let barGraphsOption = {
                    chart: {
                        renderTo: 'container11',
                        type: 'bar'
                    },
                    title: {
                        text: 'Lecturer File uploads Statistics',
                        x: -20 //center
                    },
                    subtitle: {
                        text: 'Cut @ 2017',
                        x: -20
                    },
                    xAxis: {
                        categories: []
                    },
                    yAxis: {
                        title: {
                            text: 'FILES Counts'
                        },
                        plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }]
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>:<b>{point.y}</b> of total<br/>'
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y}'
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 100,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                        shadow: true
                    },
                    series: []
                };
function showGraphslecture_fileuploads(){
  let input   = $("#lectureFileListsFilter").val().trim();  
  input     = input == ''? 'all':input;
  showDialog();
  $.post("slave.php",{
    postshow_fileuploadLecturerGraphs : input
  },function(response_){
    hideDialog();

    if(response_ == 'empty'){
      
      
    }else{
      
  var response = $.parseJSON(response_);
  barGraphsOption.xAxis.categories = response[0]['data']; //xAxis: {categories: []}
    barGraphsOption.series[0] = response[1];
    chart = new Highcharts.Chart(barGraphsOption);
    $("#container11").slideDown("slow");  
    
    }


  });

}
var pieChartOption = {
                    chart: {
                        renderTo: 'containerPie',
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: 'Lecturer File uploads Statistics'
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>' + this.point.name + '</b>: ' + this.y+ ' times';
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function() {
                                    return '<b>' + this.point.name + '</b>: ' + this.y;
                                }
                            },
                            showInLegend: true
                        }
                    },
                    series: []
                };
function downloadsfileUploadLecturerPieChart_btter(){
  let input   = $("#lectureFileListsFilter").val().trim();  
  input     = input == ''? 'all':input;
  showDialog();
  $.post("slave.php",{
    postFilterfileuploadLecturereHighChartsPie2 : input
  },function(response_){
    hideDialog();

    if(response_ == 'empty'){
      
      
    }else{
      
  var response = $.parseJSON(response_);
                   pieChartOption.series = response;
                    chart = new Highcharts.Chart(pieChartOption);
                    $("#containerPie").slideDown("slow");  
    
    }


  });
}
</script>

</body>
</html>
