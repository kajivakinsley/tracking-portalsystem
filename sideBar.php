<aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">                
                  
                  <?php if($_SESSION['userType'] == "student"){ ?>
                  <li <?php echo $PAGE=='index'? "class='active'" : "class=''"; ?>>
                      <a class="" href="index.php">
                          <i class="icon_house_alt"></i>
                          <span>Home</span>
                      </a>
                  </li>
                   <li>
                      <a class="" href="modules.php">
                          <i class="icon_genius"></i>
                          <span>Modules</span>
                      </a>
                  </li>
                  <li>
                      <a class="" href="results.php">
                          <i class="icon_document_alt"></i>
                          <span>Results</span>
                      </a>
                  </li>

                  <li>
                      <a class="" href="busary.php">
                          <i class="icon_genius"></i>
                          <span>My Bursary</span>
                      </a>
                  </li>
                   <li>
                      <a class="" href="profile.php">
                          <i class="icon_genius"></i>
                          <span>My Profile</span>
                      </a>
                  </li>
                   <li>
                      <a class="" href="library.php">
                          <i class="icon_genius"></i>
                          <span>Library</span>
                      </a>
                  </li>
                   <li>
                      <a class="" href="chat.php">
                          <i class="icon_genius"></i>
                          <span>Chats</span>
                      </a>
                  </li>
                   <li>
                      <a class="" href="">
                          <i class="icon_genius"></i>
                          <span>Library Debts</span>
                      </a>
                  </li>
                   <li>
                      <a class="" href="">
                          <i class="icon_genius"></i>
                          <span>Online Transcript</span>
                      </a>
                  </li>
                   <li>
                      <a class="" href="">
                          <i class="icon_genius"></i>
                          <span>Medical Aid Details</span>
                      </a>
                  </li>

                  <?php } if($_SESSION['userType'] == "lecturer"){  ?>
                  <li <?php echo $PAGE=='index'? "class='active'" : "class=''"; ?>>
                      <a class="" href="index.php">
                          <i class="icon_house_alt"></i>
                          <span>Home</span>
                      </a>
                  </li>
                  <li>
                      <a class="" href="uploadfileModules.php">
                          <i class="icon_genius"></i>
                          <span>UploadFiles</span>
                      </a>
                  </li>
                  <li>
                      <a class="" href="reportDownload.php">
                          <i class="icon_genius"></i>
                          <span>Logs File</span>
                      </a>
                  </li>
                  <li>
                      <a class="" href="chat.php">
                          <i class="icon_genius"></i>
                          <span>Chats</span>
                      </a>
                  </li>
                  <?php } if($_SESSION['userType'] == "admin"){  ?>
                    <li>
                          <a class="" href="admin_index.php">
                              <i class="icon_genius"></i>
                              <span>Home</span>
                          </a>
                      </li>
                      <li>
                          <a class="" href="dailyreports.php">
                              <i class="icon_genius"></i>
                              <span>S-Daily Reports</span>
                          </a>
                      </li>
                      <li>
                          <a class="" href="reportDownload.php">
                              <i class="icon_genius"></i>
                              <span>Download Logs</span>
                          </a>
                      </li>
                       <li>
                          <a class="" href="logins.php">
                              <i class="icon_genius"></i>
                              <span>Login Logs</span>
                          </a>
                      </li>
                      <li>
                          <a class="" href="Assignmentslogs.php">
                              <i class="icon_genius"></i>
                              <span>Assignments Logs</span>
                          </a>
                      </li>
                      <li>
                          <a class="" href="l_fileuplods.php">
                              <i class="icon_genius"></i>
                              <span>L-File-Uploads</span>
                          </a>
                      </li>
                     
                       <li class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="icon_desktop"></i>
                          <span>Users</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                          <li><a class="" href="adduser.php">Add</a></li>
                          <li><a class="" href="editusers.php">Edit</a></li>
                         
                      </ul>
                  </li>
                      
                      <li>
                          <a style="display: none;" class="" href="admin_index.php">
                              <i class="icon_genius"></i>
                              <span>Lecturers Logs</span>
                          </a>
                      </li>

                  <?php } ?>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>