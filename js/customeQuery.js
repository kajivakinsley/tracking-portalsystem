/****************************************************************************/


/****************************************************************************/








/****************************************************************************/





/****************************************************************************/



/****************************************************************************/




/****************************************************************************/

    
/****************************************************************************/

/****************************************************************************/
$("#hide_downloadcsv").click(function(){
	let layout = $("#downloadDiv").hide("slow");
});

/****************************************************************************/


$("#downloadSearch").keyup(function(){
	let pageSearhPageName =  $(this).val().trim();

		getDaillyReportDownloads(pageSearhPageName==''?"all":pageSearhPageName ,"level-all" , "all");
	
});




/****************************************************************************/



$("#pageSearhPageName").keyup(function(){
	let pageSearhPageName =  $(this).val().trim();
	let regNumber = $("#regNumberSearch").val().trim();

	
		getDaillyReport(regNumber==''?"all":regNumber , pageSearhPageName , "all-Date");
	
});



/****************************************************************************/


$("#regNumberSearch").keyup(function(){
	let regNumber =  $(this).val().trim();
	
		getDaillyReport(regNumber==''?"all":regNumber , "level-all" , "all-Date");
	
});


/****************************************************************************/
$("#lecturerUploadFileDoc").click(function(){
	let programSelect 	= $("#programSelect").val();
	let docName 		= $("#docName").val().trim();
	let user 			= $("#GlobalUserReadFrom").text();
	
	if(programSelect == 'null'){
		BootstrapDialog.alert({
				title: 'Select a Module ',
				message: "To upload a file you need to specify module group",
				type: BootstrapDialog.TYPE_WARNING,
				closable: true,
				draggable: true,
				buttonLabel: 'Ok'
			});
		return;
	}
	if(docName == ''){
		BootstrapDialog.alert({
				title: 'Name field Empty ',
				message: "To upload a file you need to specify Document Name",
				type: BootstrapDialog.TYPE_WARNING,
				closable: true,
				draggable: true,
				buttonLabel: 'Ok'
			});
		return;
	}
	showDialog();
	let file = _("lecturerfile").files[0];
	let formdata = new FormData();
	formdata.append("lecturerfile", file);
	formdata.append("posTdocName", docName);
	formdata.append("posTprogramSelect", programSelect);
	formdata.append("posTuser", user);
	let ajax = new XMLHttpRequest();
	ajax.upload.addEventListener("progress", progressHandlerLectureFileUpload, false);
	ajax.addEventListener("load", completeHandlerLectureFileUpload, false);
	ajax.addEventListener("error", errorHandlerLectureFileUpload, false);
	ajax.addEventListener("abort", abortHandlerLectureFileUpload, false);
	ajax.open("POST", "slave.php");
	ajax.send(formdata);


});

/****************************************************************************/

$(".uploadAssignment").click(function(){
	let modulName   = $(this).attr("CModuleCode");	
	
	let user 		= $("#GlobalUserReadFrom").text();
	
	let UserType 	= $("#GlobalUserTypeReadFrom").text();
	let ModuleCode 	= $("#fileAssignment").attr('name')	;

	let file 		= _("fileAssignment").files[0];	
	let file_ 		= _("fileAssignment").files.length ;

	if(file_ == 0){
		BootstrapDialog.alert({
				title: 'Cant no submit',
				message: "Your Please select a file !",
				type: BootstrapDialog.TYPE_WARNING,
				closable: true,
				draggable: true,
				buttonLabel: 'Ok'
			});

		return;
	}
	let formdata = new FormData();
	let ajax = new XMLHttpRequest();
	formdata.append("postAssigmentUploadFile", file);
	formdata.append("postUploadmodulName", ModuleCode);
	formdata.append("postUploaduser", user);
	formdata.append("postUploadUserType", UserType);	
	ajax.addEventListener("load", completeHandlerStudentFileUpload, false);
	ajax.addEventListener("error", errorHandlerStudentFileUpload, false);
	ajax.addEventListener("abort", abortHandlerStudentFileUpload, false);
	ajax.open("POST", "slave.php");
	ajax.send(formdata);
	showDialog();
	

	
});


/****************************************************************************/

$(".donwloadFileModuleFile").click(function(){
	fileDownload( $(this).attr("CModuleCode") ,$(this).attr("href"));
});

/****************************************************************************/
$(".CReadingMaterial").click(function(){

	let tr = $(this).closest(".trw");	
	let tr_11 = tr.next().next().next();	
	
	if (tr_11.attr("class") == 'viewCReadingMaterial') {		
		if (tr_11.is(":hidden")) {
			tr_11.show("slow");
		} else {
			tr_11.hide('slow');
		}
	}


});

/****************************************************************************/

$(".CAssignments").click(function(){

	let tr = $(this).closest(".trw");	
	let tr_11 = tr.next().next();	
	
	if (tr_11.attr("class") == 'viewAssignments') {		
		if (tr_11.is(":hidden")) {
			tr_11.show("slow");
		} else {
			tr_11.hide('slow');
		}
	}


});

/****************************************************************************/

$(".Cswk").click(function(){

	let tr = $(this).closest(".trw");
	if (tr.next().is(".viewCswk")) {
		if (tr.next().is(":hidden")) {
			$(tr).next().show("slow");
		} else {
			$(tr).next().hide('slow');
		}
	}


});


/****************************************************************************/
/*$(function() {
$("td[colspan=6]").find("p").hide();
	$("table").click(function(event) {
        event.stopPropagation();
        var $target = $(event.target);
        if ( $target.closest("td").attr("colspan") > 1 ) {
            $target.slideUp();
        } else {
            $target.closest("tr").next().find("p").slideToggle();
        }                    
    });
});*/
/****************************************************************************/
$("#loginAccount").click(function(){

	 let regnumber   = $("#loginregNumber").val().trim() ;
	   let password    = $("#loginpassword").val().trim() ;	  
	   let createAccountType = $("#createAccountType").val();
	 
	   let status      = $("#status");
	   if(createAccountType == 'null'){
	   	BootstrapDialog.alert({
					title      : 'Failed to submit ',
					message    : 'Select a user type in the drop down menu!',
					type       : BootstrapDialog.TYPE_WARNING,
					closable   : true,
					draggable  : true,
					buttonLabel: 'Ok'
				});	
	   	return;
	   }
	   if(regnumber == '' || password == '' ){
		   status.text("Submission Failed . Enter Details required!!");   
		   status.css("color", "red");	
		   status.css("font-size","18px");	
			BootstrapDialog.alert({
					title      : 'Failed to submit ',
					message    : 'Submission Failed . Enter Details required !!',
					type       : BootstrapDialog.TYPE_WARNING,
					closable   : true,
					draggable  : true,
					buttonLabel: 'Ok'
			});	
	   }else{
	   	showDialog();	   	
	   	$.post("slave.php",{ 

				   loginAccount_student_Post  : "logsinend",
				    loginregnumber                  : regnumber,
				    loginpassword                	  : password,
				    loginuserTypePost				  : createAccountType
			   },
			   function(php_response){	
			   		hideDialog();		   		
			   		
			   		if(php_response == 'unfound'){
			   			BootstrapDialog.alert({
											title      : 'Login failed ',
											message    : "Please use your correct details.\n Please try Again",
											type       : BootstrapDialog.TYPE_DANGER  ,
											closable   : true,
											draggable  : true,
											buttonLabel: 'Close'
						});	
			   		}
			   		
			   		if(php_response == 'found' && createAccountType == 'admin'){

			   			window.location.replace("admin_index.php");
			   			return;
			   		}
			   		if(php_response == 'found'){
			   			window.location.replace("index.php");
			   		}


						

			});//endof ajax	
	   
	   }
});

/****************************************************************************/


$("#addUserAdmin").click(function(){	
	
	let regnumber = $("#regNumber").val().trim();
	let password = $("#password").val().trim();
	
	let createAccountType = $("#createAccountType").val();
	
	let stdprograms = $("#stdprograms").val().trim();
	let stdlevel = $("#stdlevel").val().trim();
	let firstname =  $("#firstname").val().trim();
	let lastname =  $("#lastname").val().trim();
	   if(createAccountType == 'null'){
	   	BootstrapDialog.alert({
					title      : 'Failed to submit ',
					message    : 'Select a user type in the drop down menu!',
					type       : BootstrapDialog.TYPE_WARNING,
					closable   : true,
					draggable  : true,
					buttonLabel: 'Ok'
				});	
	   	return;
	   }
	   if(regnumber == '' || password == '' ){
		   status.text("Submission Failed . Enter Details required!!");   
		   status.css("color", "red");	
		   status.css("font-size","18px");	
			BootstrapDialog.alert({
					title      : 'Failed to submit ',
					message    : 'Submission Failed . Enter Details required !!',
					type       : BootstrapDialog.TYPE_WARNING,
					closable   : true,
					draggable  : true,
					buttonLabel: 'Ok'
			});	
	   }else{
		   
		   	if(createAccountType == 'student' ){
				if ( lastname == '' ) {
					BootstrapDialog.alert({
						title: 'Failed to submit ',
						message: 'Last Name fields is empty !!',
						type: BootstrapDialog.TYPE_WARNING,
						closable: true,
						draggable: true,
						buttonLabel: 'Ok'
					});
					return;
	  			 }
	  			 if (firstname == ''  ) {
					BootstrapDialog.alert({
						title: 'Failed to submit ',
						message: 'First Name field is empty !!',
						type: BootstrapDialog.TYPE_WARNING,
						closable: true,
						draggable: true,
						buttonLabel: 'Ok'
					});
					return;
	  			 }
	  			 if (stdprograms == 'null'  ) {
					BootstrapDialog.alert({
						title: 'Failed to submit ',
						message: 'Select Programe !!',
						type: BootstrapDialog.TYPE_WARNING,
						closable: true,
						draggable: true,
						buttonLabel: 'Ok'
					});
					return;
	  			 }
	  			 if (stdlevel == 'null'  ) {
					BootstrapDialog.alert({
						title: 'Failed to submit ',
						message: 'Select Student Level !!',
						type: BootstrapDialog.TYPE_WARNING,
						closable: true,
						draggable: true,
						buttonLabel: 'Ok'
					});
					return;
	  			 }
		   			   
		   	showDialog();			
				$.post("slave.php", {

						createAccount_student_Post  : "send",
						regnumber 					: regnumber,
						conPassword 				: password,
						userTypePost 				: createAccountType,
						firstname 					:firstname,
						lastname 					:lastname,
						stdprograms 				:stdprograms,
						stdlevel 					:stdlevel
					},
					function(php_response) {
						hideDialog();
						if (php_response == 'alreadyExist') {
							BootstrapDialog.alert({
								title: 'Will not save  ',
								message: "This user Already exist,Please Log in",
								type: BootstrapDialog.TYPE_WARNING,
								closable: true,
								draggable: true,
								buttonLabel: 'Close'
							});
						}
						if (php_response == 'saved') {
							BootstrapDialog.alert({
								title: 'New User Saved ',
								message: "User Saved .",
								type: BootstrapDialog.TYPE_SUCCESS,
								closable: true,
								draggable: true,
								buttonLabel: 'Ok'
							});
							$('#fomFileUpload')[0].reset();
						}
						if (php_response == 'error') {
							BootstrapDialog.alert({
								title: 'Database Error ',
								message: "An error has occured during saving .\n Please try Again",
								type: BootstrapDialog.TYPE_DANGER,
								closable: true,
								draggable: true,
								buttonLabel: 'Close'
							});
						}



					}); //endof ajax	
			
		   }
	   }
});




/****************************************************************************/
$("#createAccount").click(function(){	
	
	let regnumber = $("#regNumber").val().trim();
	let password = $("#password").val().trim();
	let conPassword = $("#conpassword").val().trim();
	let createAccountType = $("#createAccountType").val();
	let status = $("#status");
	let stdprograms = $("#stdprograms").val().trim();
	let stdlevel = $("#stdlevel").val().trim();
	let firstname =  $("#firstname").val().trim();
	let lastname =  $("#lastname").val().trim();
	   if(createAccountType == 'null'){
	   	BootstrapDialog.alert({
					title      : 'Failed to submit ',
					message    : 'Select a user type in the drop down menu!',
					type       : BootstrapDialog.TYPE_WARNING,
					closable   : true,
					draggable  : true,
					buttonLabel: 'Ok'
				});	
	   	return;
	   }
	   if(regnumber == '' || password == '' || conPassword == '' ){
		   status.text("Submission Failed . Enter Details required!!");   
		   status.css("color", "red");	
		   status.css("font-size","18px");	
			BootstrapDialog.alert({
					title      : 'Failed to submit ',
					message    : 'Submission Failed . Enter Details required !!',
					type       : BootstrapDialog.TYPE_WARNING,
					closable   : true,
					draggable  : true,
					buttonLabel: 'Ok'
			});	
	   }else{
		   if(password != conPassword ){
			   status.text("Submission Failed . Passwords Dont Match!");
			   status.css("color", "red");	
			   BootstrapDialog.alert({
					title      : 'Failed to submit ',
					message    : 'Passwords Dont Match !!',
					type       : BootstrapDialog.TYPE_WARNING,
					closable   : true,
					draggable  : true,
					buttonLabel: 'Ok'
				});	
		   }else{
		   	if(createAccountType == 'student' ){
				if ( lastname == '' ) {
					BootstrapDialog.alert({
						title: 'Failed to submit ',
						message: 'Last Name fields is empty !!',
						type: BootstrapDialog.TYPE_WARNING,
						closable: true,
						draggable: true,
						buttonLabel: 'Ok'
					});
					return;
	  			 }
	  			 if (firstname == ''  ) {
					BootstrapDialog.alert({
						title: 'Failed to submit ',
						message: 'First Name field is empty !!',
						type: BootstrapDialog.TYPE_WARNING,
						closable: true,
						draggable: true,
						buttonLabel: 'Ok'
					});
					return;
	  			 }
	  			 if (stdprograms == 'null'  ) {
					BootstrapDialog.alert({
						title: 'Failed to submit ',
						message: 'Select Programe !!',
						type: BootstrapDialog.TYPE_WARNING,
						closable: true,
						draggable: true,
						buttonLabel: 'Ok'
					});
					return;
	  			 }
	  			 if (stdlevel == 'null'  ) {
					BootstrapDialog.alert({
						title: 'Failed to submit ',
						message: 'Select Student Level !!',
						type: BootstrapDialog.TYPE_WARNING,
						closable: true,
						draggable: true,
						buttonLabel: 'Ok'
					});
					return;
	  			 }
		   	}
		   	status.css("color", "blue");	
		   	status.text("Sign up");
		   status.css("font-size","18px");	
		   	showDialog();
			
				$.post("slave.php", {

						createAccount_student_Post: "send",
						regnumber: regnumber,
						conPassword: conPassword,
						userTypePost: createAccountType,
						firstname:firstname,
						lastname:lastname,
						stdprograms:stdprograms,
						stdlevel:stdlevel
					},
					function(php_response) {
						hideDialog();
						if (php_response == 'alreadyExist') {
							BootstrapDialog.alert({
								title: 'Will not save  ',
								message: "This user Already exist,Please Log in",
								type: BootstrapDialog.TYPE_WARNING,
								closable: true,
								draggable: true,
								buttonLabel: 'Close'
							});
						}
						if (php_response == 'saved') {
							BootstrapDialog.alert({
								title: 'User Saved ',
								message: "User Saved .Use your details to Log in.",
								type: BootstrapDialog.TYPE_SUCCESS,
								closable: true,
								draggable: true,
								buttonLabel: 'Ok'
							});
						}
						if (php_response == 'error') {
							BootstrapDialog.alert({
								title: 'Database Error ',
								message: "An error has occured during saving .\n Please try Again",
								type: BootstrapDialog.TYPE_DANGER,
								closable: true,
								draggable: true,
								buttonLabel: 'Close'
							});
						}



					}); //endof ajax	
			
		   }
	   }
});



/*-------------------------------------------------------------------------------------------------------------------------*/

$("#detailsDetails").click(function(){
	let _surname = $("#surname").val();
	let _name = $("#name").val();
	let _password = $("#password").val();
	let _level = $("#level").val();
	let regnumber = $("#regnumber").text();	

	if(_surname == '' || _name == '' ||_password == '' ||_level == ''   ){
		BootstrapDialog.alert({
			title: 'Can not save',
			message: "some of the fields are empty !.",
			type: BootstrapDialog.TYPE_WARNING,
			closable: true,
			draggable: true,
			buttonLabel: 'Ok'
		});
		return;
	}
	showDialog();
	$.post("slave.php",{
		postupDateFile	:"search",
		post_name  : _name,
		post_surname  : _surname,
		post_password  : _password,
		post_level  : _level,
		postregnumber : regnumber

	},function(response){		
		hideDialog();		
		if(response == 'done' ){
			getAllStudentsforEditStudent(edistSeachVal);
				BootstrapDialog.alert({
			title: 'Saved',
			message: "Details updated  !.",
			type: BootstrapDialog.TYPE_DANGER,
			closable: true,
			draggable: true,
			buttonLabel: 'Ok'
		});
		$('#fomFileUpload')[0].reset();
		}else{
			BootstrapDialog.alert({
			title: 'Can not save',
			message: "something went wrong  !.",
			type: BootstrapDialog.TYPE_DANGER,
			closable: true,
			draggable: true,
			buttonLabel: 'Ok'
		});
		}

	});

});
var edistSeachVal = "";


/************************************************************************/
function deletuser(){
	deleteUserReg(arguments[0]);
	/*BootstrapDialog.show({
            message: 'Delete Confirmation for '+arguments[0],
            buttons: [
            {
                label: 'Delete this user',
                action: function(dialogItself){
                    dialogItself.close();
                   // deleteUserReg(arguments[0]);
                }
            }]
        });*/
}
/**/
function deleteUserReg(){
	showDialog();
	$.post("slave.php", {
					poststdDelete: arguments[0]

				}, function(response) {
					hideDialog();
					alert(response);

					if(response == 'done'){
						BootstrapDialog.show({
							message: 'User Removed'
						});
						getAllStudentsforEditStudent(edistSeachVal);
					}
					//layout.html(response);

				});
}

/****************************************************************************/

function edituser(){
	showDialog();
	let regnumber = arguments[0];
	let surname =  arguments[1];
	let name =  arguments[2];
	let password =  arguments[3];
	let date_created =  arguments[4];
	let level =  arguments[5];
	let program =  arguments[6];
	/*------*/
	let _surname = $("#surname").val(surname);
	let _name = $("#name").val(name);
	let _password = $("#password").val(password);
	let _level = $("#level").val(level);
	$("#regnumber").text(regnumber);	
	hideDialog();
}
/*************************************************************************88*/

/*************************************************************************88*/
function closeFilter(){
	

	$("#divfilterResultBarChart").hide("slide", { direction: "right" }, 3200 , function(){
		$("#showfilterResultsTable").slideUp("slow");
	});
}

/*************************************************************************88*/
function showDownloadGraphs(){	
	
	$("#graphDaillyreportFunctions").slideUp("slow", function() {

		$("#showgraphsDaillyReports").slideDown(400, function() {
			showHighDownloadsCharts("all"); 
		});
	});	
}


/*************************************************************************88*/

function backDownloadreport(){
	$("#showgraphsDaillyReports").slideUp("slow", function() {
		$("#graphDaillyreportFunctions").slideDown("slow");
		$("#container11").slideUp("slow");
	});
	
}

/****************************************************************************/
function backdailyreport(){
	$("#showgraphsDaillyReports").slideUp("slow", function() {
		$("#graphDaillyreportFunctions").slideDown("slow");
	});
	
}
/****************************************************************************/
function showDailyReportGraphs(){	
	
	$("#graphDaillyreportFunctions").slideUp("slow", function() {

		$("#showgraphsDaillyReports").slideDown(400, function() {
			showHighCharts("all");
		});
	});	
}
/****************************************************************************/
function downloadsPieChart(){
	$.post("slave.php",{
		postFilterRequestDownloadHighCharts	:"search",
		postallAttributs : "pie"

	},function(response){
	//$("#container11").slideUp();
	//$("#container222").slideDown();
	
	var options = {
                    chart: {
                        renderTo: 'container222',
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: 'Web Sales & Marketing Efforts'
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function() {
                                    return '<b>' + this.point.name + '</b>: ' + this.y;
                                }
                            },
                            showInLegend: true
                        }
                    },
                    series: []
                };
		// $("#container222").html(response);		
		options.series = response;
        chart = new Highcharts.Chart(options);

	});
}
/****************************************************************************/
function downloadsDonutChart(){
	$.post("slave.php",{
		postFilterRequestDownloadHighCharts	:"search",
		postallAttributs : "pie"

	},function(response){
	$("#container11").slideUp();
		$("#container222").html(response);		

	});
}
/****************************************************************************/

function showHighDownloadsCharts(){	
	let barGraphsOption = {
                    chart: {
                        renderTo: 'container11',
                        type: 'bar'
                    },
                    title: {
                        text: 'Downloads Statistics',
                        x: -20 //center
                    },
                    subtitle: {
                        text: 'Cut @ 2017',
                        x: -20
                    },
                    xAxis: {
                        categories: []
                    },
                    yAxis: {
                        title: {
                            text: 'Download Counts'
                        },
                        plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }]
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>:<b>{point.y}</b> of total<br/>'
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y}'
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 100,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                        shadow: true
                    },
                    series: []
                };
	let _search = $("#downloadSearch").val().trim();
	_search   = _search == ''? 'all':_search;
	edistSeachVal = _search;

	$.post("slave.php",{
		postFilterRequestDownloadHighCharts2	:_search

	},function(response_){		
		
    
    	var response = $.parseJSON(response_);
    	barGraphsOption.xAxis.categories = response[0]['data']; //xAxis: {categories: []}
        barGraphsOption.series[0] = response[1];
        chart = new Highcharts.Chart(barGraphsOption);
        $("#container11").slideDown("slow");	
        

	});

}
/****************************************************************************/

function downloadsPieChart_btter(){	
	var pieChartOption = {
                    chart: {
                        renderTo: 'containerPie',
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: 'Download Statistics'
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>' + this.point.name + '</b>: ' + this.y+ ' Pengunjung';
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function() {
                                    return '<b>' + this.point.name + '</b>: ' + this.y;
                                }
                            },
                            showInLegend: true
                        }
                    },
                    series: []
                };
	let _search = $("#downloadSearch").val().trim();
	_search   = _search == ''? 'all':_search;
	edistSeachVal = _search;

	$.post("slave.php",{
		postFilterRequestDownloadHighChartsPie2	:_search

	},function(response_){		
                
                	var response = $.parseJSON(response_);
                	 pieChartOption.series = response;
                    chart = new Highcharts.Chart(pieChartOption);
                    $("#containerPie").slideDown("slow");	
                    

	});

}
/****************************************************************************/

function btnlectureFileLists(){
	let input   = $("#lectureFileListsFilter").val().trim();
	let layout  = $("#show_fileupload");
	input 		= input == ''? 'all':input;
showDialog();
	$.post("slave.php",{
		postshow_fileuploadLecturer : input
	},function(response){
		hideDialog();
		if(response == 'empty'){
			layout.html("<tr colspan='6'> <td>  <center> <span style='color:red;'> Nuthing Found !!!  </span> </center> </td> </tr>");
		}else{
			layout.html(response);
		}


	});

}


/****************************************************************************/

function btnlectureFileListsReset(){
	let layout  = $("#show_fileupload");
showDialog();
	$.post("slave.php",{
		postshow_fileuploadLecturer : "all"
	},function(response){
		hideDialog();
		if(response == 'empty'){
			
			layout.html("<tr colspan='5'> <center> <span style='color:red;'> Nuthing Found !!!  </span> </center> </tr>");
		}else{
			layout.html(response);
		}


	});
}

/****************************************************************************/


/****************************************************************************/



/****************************************************************************/
function showHightChartsDailyPieCharts(data_1){
	var dataArray = $.parseJSON(data_1);			
		
		let index_array = null;
		let results_array = null;
		let Modules_array = null;
		let sum_profile = null;
		let sum_busary = null;

		$.each(dataArray , function(i , val ){			
			if(i == 0){
				index_array = val;
			}
			if(i == 1){
				results_array = val;
			}
			if(i == 2){
				Modules_array = val;
			}
			if(i == 3){
				sum_profile = val;
			}
			if(i == 4){
				sum_busary = val;
			}

		});
	
        Highcharts.chart('container222', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Daily Activity Reviews'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Page visits',
        colorByPoint: true,
        data: [{
            name: 'Index page',
            y: index_array[1]
        }, {
            name: 'Results',
            y: results_array[1],
            sliced: true,
            selected: true
        }, {
            name: 'Modules',
            y: Modules_array[1]
        }, {
            name: 'Profile',
            y: sum_profile[1]
        }, {
            name: 'Bursary',
            y: sum_busary[1]
        }]
    }]
});

}
/****************************************************************************/


/****************************************************************************/


function dailyReportBasicArea(data_){
	var dataArray = $.parseJSON(data_1);			
		
		let index_array = null;
		let results_array = null;
		let Modules_array = null;
		let sum_profile = null;
		let sum_busary = null;

		$.each(dataArray , function(i , val ){			
			if(i == 0){
				index_array = val;
			}
			if(i == 1){
				results_array = val;
			}
			if(i == 2){
				Modules_array = val;
			}
			if(i == 3){
				sum_profile = val;
			}
			if(i == 4){
				sum_busary = val;
			}

		});
		// start of graphing data
		Highcharts.chart('container333', {
    chart: {
        type: 'area'
    },
    title: {
        text: 'Daily Activity Reports'
    },
    subtitle: {
        text: 'Source: Cut @ 2017'
    },
    xAxis: {
        allowDecimals: false,
        labels: {
            formatter: function () {
                return this.value; // clean, unformatted number for year
            }
        }
    },
    yAxis: {
        title: {
            text: 'Visists'
        },
        labels: {
            formatter: function () {
                return this.value / 1000 + 'k';
            }
        }
    },
    tooltip: {
        pointFormat: '{series.name} pages <b>{point.y:,.0f}</b><br/>vists in {point.x}'
    },
    plotOptions: {
        area: {
            pointStart: 1940,
            marker: {
                enabled: false,
                symbol: 'circle',
                radius: 2,
                states: {
                    hover: {
                        enabled: true
                    }
                }
            }
        }
    },
    series: [{
        name: 'USA',
        data: [null, null, null, null, null, 6, 11, 32, 110, 235, 369, 640,
            1005, 1436, 2063, 3057, 4618, 6444, 9822, 15468, 20434, 24126,
            27387, 29459, 31056, 31982, 32040, 31233, 29224, 27342, 26662,
            26956, 27912, 28999, 28965, 27826, 25579, 25722, 24826, 24605,
            24304, 23464, 23708, 24099, 24357, 24237, 24401, 24344, 23586,
            22380, 21004, 17287, 14747, 13076, 12555, 12144, 11009, 10950,
            10871, 10824, 10577, 10527, 10475, 10421, 10358, 10295, 10104]
    }, {
        name: 'USSR/Russia',
        data: [null, null, null, null, null, null, null, null, null, null,
            5, 25, 50, 120, 150, 200, 426, 660, 869, 1060, 1605, 2471, 3322,
            4238, 5221, 6129, 7089, 8339, 9399, 10538, 11643, 13092, 14478,
            15915, 17385, 19055, 21205, 23044, 25393, 27935, 30062, 32049,
            33952, 35804, 37431, 39197, 45000, 43000, 41000, 39000, 37000,
            35000, 33000, 31000, 29000, 27000, 25000, 24000, 23000, 22000,
            21000, 20000, 19000, 18000, 18000, 17000, 16000]
    }]
});
	
}

/****************************************************************************/
function donutGraph (data_1) {
	var dataArray = $.parseJSON(data_1);			
		
		let index_array = null;
		let results_array = null;
		let Modules_array = null;
		let sum_profile = null;
		let sum_busary = null;

		$.each(dataArray , function(i , val ){			
			if(i == 0){
				index_array = val;
			}
			if(i == 1){
				results_array = val;
			}
			if(i == 2){
				Modules_array = val;
			}
			if(i == 3){
				sum_profile = val;
			}
			if(i == 4){
				sum_busary = val;
			}

		});
	Highcharts.chart('container333', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: 0,
			plotShadow: false
		},
		title: {
			text: 'Pages<br>Visists<br>2017',
			align: 'center',
			verticalAlign: 'middle',
			y: 40
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions: {
			pie: {
				dataLabels: {
					enabled: true,
					distance: -50,
					style: {
						fontWeight: 'bold',
						color: 'white'
					}
				},
				startAngle: -90,
				endAngle: 90,
				center: ['50%', '75%']
			}
		},
		series: [{
			type: 'pie',
			name: 'Page Visists',
			innerSize: '50%',
			data: [
				['Index', index_array[1]],
				['Results',results_array[1]],
				['Bursary', sum_busary[1]],
				['Profile', sum_profile[1]],
				['Modules', Modules_array[1]], {
					name: 'Proprietary or Undetectable',
					y: 0.2,
					dataLabels: {
						enabled: false
					}
				}
			]
		}]
	});
}
/****************************************************************************/
function dailyReportLineGrapgh(){
	$("#container11").slideDown(400,function(){

		$("#container222").slideUp("slow");	
		$("#container333").slideUp("fast");	

	});

	

}
/****************************************************************************/
function dailyPieChart(){
	$("#container222").slideDown(400,function(){
			$("#container11").slideUp("slow");	
			$("#container333").slideUp("fast");	
	});
	
}

/****************************************************************************/
function dailyDonutChart(){
	$("#container333").slideDown(400,function(){
			$("#container11").slideUp("slow");	
			$("#container222").slideUp("slow");	
	});
	
}
/****************************************************************************/
var GlobalDailyCharts_Hidden_data = "";
/****************************************************************************/
function showHighCharts(search){
	edistSeachVal = search;
	
	$.post("slave.php",{
		postFilterRequestHighCharts	:search,
		postallAttributs : "all"

	},function(response){		
		
		$("#container11").html(response);		
		GlobalDailyCharts_Hidden_data = $("#hiddenData").text();
		showHightChartsDailyPieCharts(GlobalDailyCharts_Hidden_data);		
		donutGraph(GlobalDailyCharts_Hidden_data)
		//alert(GlobalDailyCharts_Hidden_data);
		
		
		

	});

}


/****************************************************************************/


function getAllStudentsforEditStudent(search){
	edistSeachVal = search;
	showDialog();
	let layout = $("#showDiv");
	$.post("slave.php",{
		postsearch	:search

	},function(response){		
		hideDialog();
		
		layout.html(response);

	});

}

/****************************************************************************/


function printDailyReport(){
	var divToPrint=document.getElementById("tableID");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}

/****************************************************************************/

function excelfy(){
	showDialog();
	$.post("slave.php",{
		postDaillyReportxel	:"post",
		postDaillyReportxelbyBy			:Global_byBy,
		postDaillyReportxelallAttributs	:GlobalallAttributs,
		postDaillyReportxeldateFilter		:Global_dateFilter		

	},function(response){		
		hideDialog();	

	});

    }

/****************************************************************************/
var GlobalDownloadCSV="";
function csvfy(){
	showDialog();
	$.post("slave.php",{
		postDaillyReportcsv	:"post",
		postDaillyReportcsvbyBy			:Global_byBy,
		postDaillyReportcsvallAttributs	:GlobalallAttributs,
		postDaillyReportcsvdateFilter		:Global_dateFilter		

	},function(response){		
		hideDialog();
		//alert(response);
		
		let layout = $("#downloadDiv").show("slow");
		layout.html("<a href='"+response+"' > DownLoad CSV File </a> ");
		$("#hide_downloadcsv").show('fast');
		GlobalDownloadCSV = response;
	


	});

    }

/****************************************************************************/
function csvfyDownload(){
	showDialog();
	$.post("slave.php",{
		postDaillyReportDowloadcsv	:"post",
		postDaillyReportDowloadcsvbyBy			:Global_byBy,
		postDaillyReportDowloadcsvallAttributs	:GlobalallAttributs,
		postDaillyReportDowloadcsvdateFilter		:Global_dateFilter		

	},function(response){		
		hideDialog();
		//alert(response);
		
		let layout = $("#downloadDiv").show("slow");
		layout.html("<a href='"+response+"' > DownLoad CSV File </a>");
		GlobalDownloadCSV = response;
	


	});

    }

/****************************************************************************/




var GlobalallAttributs = "" , Global_byBy = "" , Global_dateFilter = "" , GlobalfilterBy = "" ;
/****************************************************************************/
function getFilterdata(){
	let by = $("#filderByDailyLogs").val();
	let divDisplay 		= $("#contentTableFilter");
	if(by != 'null'){
		getDaillyReportFilter(by);
	}else{
		$("#showfilterResultsTable").slideUp("slow");
		
		return;
	}
	
}
function getDaillyReportFilter(filterBy){
	GlobalfilterBy      = filterBy ;
	
	let divToShowTable = $("#contentTableFilter");
	
	showDialog();
	$.post("slave.php",{
		postFilter 			:"post",
		postfilterBy		:filterBy		

	},function(response){		
		hideDialog();	
		 $("#filderByDailyLogs").val(GlobalfilterBy);
		
		if(response == 'empty'){
			$("#showfilterResultsTable").slideUp("slow");
			//showLayout.html("<tr><td colspan='6'> <center>  <label style='color:red;margin:0 100px;'><strong> <u> No Record found !!  </u>  </strong> </label> </center> <td></tr>");	
		}else{
			$("#showfilterResultsTable").slideDown("slow");
			//alert("sss");
			
			$("#qweqweqw").html(response);
			
		}
		

	});

}



/****************************************************************************/

function getDaillyReport(allAttributs , byBy , dateFilter){
	let showLayout 		= $("#showDailyReport");
	GlobalallAttributs  = allAttributs;
	Global_byBy 		= 	byBy;
	Global_dateFilter 	= dateFilter;
	showDialog();
	$.post("slave.php",{
		postFilterRequest	:"post",
		postbyBy			:byBy,
		postallAttributs	:allAttributs,
		postdateFilter		:dateFilter		

	},function(response){		
		hideDialog();
		if(response == 'empty'){
			$("#actionDailyReports").hide("slow");
			showLayout.html("<tr><td colspan='6'> <center>  <label style='color:red;margin:0 100px;'><strong> <u> No Record found !!  </u>  </strong> </label> </center> <td></tr>");	
		}else{
			$("#actionDailyReports").show("slow");
			showLayout.html(response);	
		}
		

	});

}

/****************************************************************************/


/****************************************************************************/
function getDaillyReportDownloads(allAttributs , byBy , dateFilter){
	let showLayout 		= $("#showDailyReport");
	GlobalallAttributs  = allAttributs;
	Global_byBy 		= 	byBy;
	Global_dateFilter 	= dateFilter;
	showDialog();
	$.post("slave.php",{
		postFilterDownload	:"post",
		postbyBy			:byBy,
		postallAttributs	:allAttributs,
		postdateFilter		:dateFilter		

	},function(response){		
		hideDialog();
		if(response == 'empty'){
			$("#actionDailyReports").hide("slow");
			showLayout.html("<tr><td colspan='6'> <center>  <label style='color:red;margin:0 100px;'><strong> <u> No Record found !!  </u>  </strong> </label> </center> <td></tr>");	
		}else{
			$("#actionDailyReports").show("slow");
			showLayout.html(response);	
		}
		

	});

}


/****************************************************************************/



var GlobalLecture_File_uploaded="";

/****************************************************************************/

function getAllLectureUploads(whoseFiles , moduleCode){
GlobalLecture_File_uploaded = whoseFiles;
	$.post("slave.php",{
		postFilesUploadsLecturer	:"post",
		postWhichLecture		:whoseFiles,
		postModuleCode			:moduleCode		

	},function(response){		
		let layout = $("#showFilesuploaded");
		if (response != 'Modnuthing' || response != 'nuthing') {
			layout.html(response);
		}

	});

}


/****************************************************************************/
function completeHandlerLectureFileUpload(event){
         hideDialog();

        
          if(event.target.responseText=="Complete"){
		BootstrapDialog.alert({
			title: 'Document Saved ',
			message: "Successfully upload file.",
			type: BootstrapDialog.TYPE_SUCCESS,
			closable: true,
			draggable: true,
			buttonLabel: 'Ok'
		});

		getAllLectureUploads(GlobalLecture_File_uploaded , 'all');

			if($("#uploadedCheckBoxResetForm").is(':checked')){
	       		$('#fomFileUpload')[0].reset();
	    	}
            }else{
            	if(event.target.responseText == "exists"){
		BootstrapDialog.alert({
			title: 'Document Saved Duplication',
			message: "File Already saved.",
			type: BootstrapDialog.TYPE_WARNING,
			closable: true,
			draggable: true,
			buttonLabel: 'Ok'
		});
            	}else{
		BootstrapDialog.alert({
			title: 'Document not Saved ',
			message: "upload failed.tr again! ",
			type: BootstrapDialog.TYPE_DANGER,
			closable: true,
			draggable: true,
			buttonLabel: 'Ok'
		});
            	}
            	
            }

         
}
/****************************************************************************/
function completeHandlerStudentFileUpload(event){
         hideDialog();
         alert(event.target.responseText);
        
          if(event.target.responseText=="saved"){
				BootstrapDialog.alert({
					title: 'Document Saved ',
					message: "Successfully upload file.",
					type: BootstrapDialog.TYPE_SUCCESS,
					closable: true,
					draggable: true,
					buttonLabel: 'Ok'
				});

		//getAllLectureUploads(GlobalLecture_File_uploaded , 'all');

				
            }else{
	            	if(event.target.responseText == "exists"){
						BootstrapDialog.alert({
							title: 'Document Saved Duplication',
							message: "File Already saved.",
							type: BootstrapDialog.TYPE_WARNING,
							closable: true,
							draggable: true,
							buttonLabel: 'Ok'
						});
            	}else{
            		if(event.target.responseText == 'failedupload'){
            			alert("failedupload");
            		}
					BootstrapDialog.alert({
						title: 'Document not Saved ',
						message: "upload failed.tr again! ",
						type: BootstrapDialog.TYPE_DANGER,
						closable: true,
						draggable: true,
						buttonLabel: 'Ok'
					});
            	}
            	
            }

         
}
/****************************************************************************/

function errorHandlerLectureFileUpload(event){
   hideDialog();
	BootstrapDialog.alert({
		title: 'Document not Saved ',
		message: "An error Ocured.Please try again.",
		type: BootstrapDialog.TYPE_DANGER,
		closable: true,
		draggable: true,
		buttonLabel: 'Ok'
	});
   
}
/****************************************************************************/

function errorHandlerStudentFileUpload(event){
   hideDialog();
	BootstrapDialog.alert({
		title: 'Document not Saved ',
		message: "An error Ocured.Please try again.",
		type: BootstrapDialog.TYPE_DANGER,
		closable: true,
		draggable: true,
		buttonLabel: 'Ok'
	});
   
}
/****************************************************************************/


function abortHandlerStudentFileUpload(event){
 BootstrapDialog.alert({
		title: 'Document not Saved ',
		message: "An cancellation has ocured.Please try again to upload.",
		type: BootstrapDialog.TYPE_WARNING,
		closable: true,
		draggable: true,
		buttonLabel: 'Ok'
	});
 
}

/****************************************************************************/


function abortHandlerLectureFileUpload(event){
 BootstrapDialog.alert({
		title: 'Document not Saved ',
		message: "An cancellation has ocured.Please try again to upload.",
		type: BootstrapDialog.TYPE_WARNING,
		closable: true,
		draggable: true,
		buttonLabel: 'Ok'
	});
 
}

/****************************************************************************/


function progressHandlerLectureFileUpload(event){

}


/****************************************************************************/
function fileDownload(){

	let moduleCode  = arguments[0];
	let filename    = arguments[1];
	let user 		= $("#GlobalUserReadFrom").text();
	let pageName 	= $("#GlobalPageReadFrom").text();
	let UserType 	= $("#GlobalUserTypeReadFrom").text();	

	$.post("slave.php",{
		postfileDownload				:"post",
		postfileDownloadpageName		:pageName,
		postfileDownloaduser			:user,
		postfileDownloadUserType		:UserType ,
		postfileDownloadmoduleCode		:moduleCode,
		postfileDownloadfilename   		:filename
	},function(response){		
		console.log("fileDownload:"+response);

	});

}


/****************************************************************************/
function pagevisitLecturer(){
	let user = $("#GlobalUserReadFrom").text();
	let pageName = $("#GlobalPageReadFrom").text();
	let UserType = $("#GlobalUserTypeReadFrom").text();	
	$.post("slave.php",{
		postTrackPageVisitLecturer	:"post",
		postpageNameLecturer		:pageName,
		postuserLecturer			:user,
		postUserTypeLecturer		:UserType 
	},function(response){		
		console.log("Lecture pagevisit:"+response);
	});
}

/****************************************************************************/
function pagevisitStudent(){
	let user = $("#GlobalUserReadFrom").text();
	let pageName = $("#GlobalPageReadFrom").text();
	let UserType = $("#GlobalUserTypeReadFrom").text();	
	$.post("slave.php",{
		postTrackPageVisit	:"post",
		postpageName		:pageName,
		postuser			:user,
		postUserType		:UserType 
	},function(response){		
		console.log("pagevisit:"+response);
	});
}
/****************************************************************************/


function iconChangeAdmin(){
	let createAccountType = $("#createAccountType").val();

	if (createAccountType == 'student') {		
		$("#stdprogramsAdmin").show("fast");
		$("#stdlevelAdmin").show("fast");
	

	}
	if (createAccountType == 'admin') {		
		$("#stdprogramsAdmin").hide("fast");
		$("#stdlevelAdmin").hide("fast");
	
	}
	if (createAccountType == 'lecturer') {
				
		$("#stdprogramsAdmin").hide("fast");
		$("#stdlevelAdmin").hide("fast");
	} 
	if (createAccountType == 'null') {

		$("#stdprogramsAdmin").hide("fast");
		$("#stdlevelAdmin").hide("fast");
	}
}


/****************************************************************************/

function iconChange(){
	let createAccountType = $("#createAccountType").val();

	if (createAccountType == 'student') {
		$("#logoSign").attr("src", "img/student-icon.png");
		$("#stdprograms").show("fast");
		$("#stdlevel").show("fast");
		$("#stdElements").show("fast");

	}
	if (createAccountType == 'admin') {
		$("#logoSign").attr("src", "img/admin-icon.png");
		$("#stdElements").hide("fast");
	}
	if (createAccountType == 'lecturer') {
		$("#logoSign").attr("src", "img/log.png");
		$("#stdlevel").hide("fast");
		$("#stdprograms").show("fast");
		$("#stdElements").show("fast");
	} 
	if (createAccountType == 'null') {
		$("#logoSign").attr("src", "img/User -1.png");
		
	}
}
/****************************************************************************/


function showDialog() {
	$.LoadingOverlay("show");
}



/***************************************************/

function hideDialog() {
	$.LoadingOverlay("hide");
}

/***********************************************************************************************/

/**This will validate email and return a boolean value*/
function ValidateEmail(inputText) {
	let mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if (inputText.match(mailFormat)) {
		return true;
	} else {
		return false;
	}
}

/**to escape html elements to avoid injections*/
let tagsToReplace = {
	'&': '&amp;',
	'<': '&lt;',
	'>': '&gt;'
};
/***********************************************************************************************/
function __replaceTag(tag) {
	return tagsToReplace[tag] || tag;
}
/***********************************************************************************************/
function safe_tags_replace(str) {
	return str.replace(/[&<>]/g, __replaceTag);
}
/* ********************************************************************************************* */
function _(el){
	return document.getElementById(el);
}
/* ********************************************************************************************* */
