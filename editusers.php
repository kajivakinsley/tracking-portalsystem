<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">
<?php $PAGE = 'Edit Users'; ?>
    <title> Admin Edit Users </title>

    <!-- Bootstrap CSS -->    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="css/elegant-icons-style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />    
    <!-- Custom styles -->
  <link rel="stylesheet" href="css/fullcalendar.css">
  <link href="css/widgets.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
  
  <link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap-dialog.min.css">
    
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
     
      
    
      <!--header end-->
 <?php  include 'header.php' ; ?>   
      <!--sidebar start-->
    <?php  include 'sidebar.php' ; ?>   
      <!--sidebar end-->
      
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">            
              <!--overview start-->
        <div class="row">
        <div class="col-lg-12">
          
          <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="admin_index.php">Home</a></li>
             <li><i class="fa fa-home"></i><a href="editusers.php">Edit Users</a></li>
            
          </ol>
        </div>
      </div>
      
      <!-- Today status end -->              
        
      <div class="row">

        <div class="col-lg-12">
                      <section class="panel" style="display: none;">
                          <header class="panel-heading">
                             Filter/Search by :
                          </header>
                          <div class="panel-body">
                              <form class="form-horizontal "  onsubmit="return false;">
                                <div class="form-group">
                                      <label class="control-label col-lg-2" for="inputSuccess">Inline checkboxes</label>
                                      <div class="col-lg-10">
                                          <label class="checkbox-inline">
                                              <input type="checkbox" id="inlineCheckbox1" value="option1"> 1
                                          </label>
                                          <label class="checkbox-inline">
                                              <input type="checkbox" id="inlineCheckbox2" value="option2"> 2
                                          </label>
                                          <label class="checkbox-inline">
                                              <input type="checkbox" id="inlineCheckbox3" value="option3"> 3
                                          </label>

                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="control-label col-lg-2" for="inputSuccess">Column sizing</label>
                                      <div class="col-lg-10">
                                          <div class="row">
                                              <div class="col-lg-2">
                                                  <input type="text" class="form-control" placeholder=".col-lg-2">
                                              </div>
                                              <div class="col-lg-3">
                                                  <input type="text" class="form-control" placeholder=".col-lg-3">
                                              </div>
                                              <div class="col-lg-4">
                                                  <input type="text" class="form-control" placeholder=".col-lg-4">
                                              </div>
                                          </div>

                                      </div>
                                  </div>
                                  
                              </form>
                          </div>
                      </section>
                     
                     
                     
                  </div>

      </div>
      <div class="row">
        <div class="col-lg-6">
        <section class="panel">
          <header class="panel-heading">
             <i class="icon_document_alt"></i>
            User Edit Form : <span id="regnumber"> </span>
          </header>
          <div class="panel-body">
            <form class="form-horizontal" id="fomFileUpload" role="form" onsubmit="return false;" autocomplete="off">

              <div class="form-group">
                <label for="inputEmail1" class="col-lg-2 control-label">Name</label>
                <div class="col-lg-10">
                  <input type="text"  class="form-control" id="name" placeholder="Name ">                  
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail1" class="col-lg-2 control-label">Surname</label>
                <div class="col-lg-10">
                  <input type="text"  class="form-control" id="surname" placeholder="Surname ">                  
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail1" class="col-lg-2 control-label">Password</label>
                <div class="col-lg-10">
                  <input type="text"  class="form-control" id="password" placeholder="password ">                  
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail1" class="col-lg-2 control-label">Level</label>
                <div class="col-lg-10">
                  <input type="text"  class="form-control" id="level" placeholder="1.2 , 4.2 ">                  
                </div>
              </div>
               <div class="row">
                
             
                <div class="col-xs-6 col-md-6">
                  <button type="submit" id="detailsDetails" class="btn btn-primary">Update</button>
                </div>
              
              </div>
            </form>
          </div>
        </section>

      </div>
       <div class="col-lg-6">
                      <section class="panel">
                          <header class="panel-heading">
                             <b><strong>User Types </strong></b>
                          </header>
                          
                        <section class="panel">
                          <header class="panel-heading tab-bg-primary ">
                              <ul class="nav nav-tabs">
                                  <li class="active">
                                      <a data-toggle="tab" href="#home">Students</a>
                                  </li>
                                  <li class="">
                                      <a data-toggle="tab" href="#Lecturers">Lecturers</a>
                                  </li>
                                  
                                
                              </ul>
                          </header>
                          <div class="panel-body">
                              <div class="tab-content">                                  
                                  <div id="home" class="tab-pane active">                                    
                                    <form class="form-horizontal " onsubmit="return false;">
                                  <div class="form-group">
                                     
                                      <div class="col-sm-8">
                                         <label class="control-label" style="display: none;" for="inputSuccess">Student Name</label><br>
                                           <input style="display: none;" class="form-control input-sm m-bot15" type="text" placeholder=".input-sm">
                                            <label style="display: none;" class="control-label" for="inputSuccess">Student Programe</label><br>
                                          <input style="display: none;" class="form-control input-sm m-bot15" type="text" placeholder=".input-sm">
                                           <label class="control-label" for="inputSuccess">Student RegNumber</label><br>
                                          <input  class="form-control input-sm m-bot15" type="text" placeholder="RegiNumber ...">
                                          <label style="display: none;" class="control-label" for="inputSuccess">Student Level</label><br>  
                                          <select style="display: none;" class="form-control input-lg m-bot15" >
                                              <option value= "null">Select Level</option>
                                                <option value="1.1">Level 1.1</option>
                                                <option  value="1.2">Level 1.2</option>
                                                <option value="2.1">Level 2.1</option>
                                                <option  value="2.2">Level 2.2</option>
                                                <option  value="3.1">Level 3.1</option>
                                                <option  value="3.2">Level 3.2</option>
                                                <option  value="4.1">Level 4.1</option>
                                                <option  value="4.2">Level 4.2</option>
                                          </select>
                                          
                                      </div>
                                  </div>
                              </form>
                              <div>
                                <table class="table table-bordered">
                                 
                                    <thead
                                    <tr>                                
                                 <th> Reg Number </th>                                 
                                  <th> Action</th>
                              </tr>
                            </thead>
                               <tbody id="showDiv">
                              
                            

                                  </tbody>

                                </table>
                              </div>
                                     
                                  <div id="Lecturers" class="tab-pane">Profile</div>
                                  
                              </div>
                          </div>
                      </section>
                      </section>
                  </div>
      </div>

                    
                   
                <!-- statics end -->
              
            
        


          </section>
         
      </section>
      <!--main content end-->
  </section>
  <!-- container section start -->

    <!-- javascripts -->
    <script src="js/jquery.js"></script>
  <script src="js/jquery-ui-1.10.4.min.js"></script>
    <script src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <!-- bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>   
    <!--custome script for all page-->
    <script src="js/scripts.js"></script>
    <!-- custom script for this page-->
    
  <script src="js/jquery.autosize.min.js"></script>
  <script src="js/jquery.placeholder.min.js"></script>
  <script src="js/gdp-data.js"></script>  
  <script src="js/morris.min.js"></script>
  
  <script src="js/jquery.slimscroll.min.js"></script>
 <!-- cutsome use -->
 <script type="text/javascript" src="js/bootstrap-dialog.min.js"></script>
   <script type="text/javascript" src="js/loadingoverlay.min.js"></script>
   <script type="text/javascript" src="js/loadingoverlay_progress.min.js"></script>
   <script src="js/customeQuery.js"></script> 

   <script type="text/javascript">
     getAllStudentsforEditStudent("all");
   </script>

  </body>
</html>
