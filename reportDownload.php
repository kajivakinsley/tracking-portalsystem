<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">
<?php $PAGE = 'Download Reports'; ?>
    <title> Download Reports page </title>

    <!-- Bootstrap CSS -->    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="css/elegant-icons-style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />    
    <!-- Custom styles -->
  <link rel="stylesheet" href="css/fullcalendar.css">
  <link href="css/widgets.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
  
  <link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap-dialog.min.css">
    
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
     
      
    
      <!--header end-->
 <?php  include 'header.php' ; ?>   
      <!--sidebar start-->
    <?php  include 'sidebar.php' ; ?>   
      <!--sidebar end-->
      
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">            
              <!--overview start-->
        <div class="row">
        <div class="col-lg-12">
          
          <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="admin_index.php">Home</a></li>
            <li><i class="fa fa-home"></i><a href="dailyreports.php">Download Reports</a></li>
            
          </ol>
        </div>
      </div>
      
      <!-- Today status end -->              
        
      <div class="row">
        <!-- start of graph -->
<div class="col-lg-12" id="showgraphsDaillyReports" style="display: none;">
                      <section class="panel">
                          <header class="panel-heading">
                           Showing Graph :
                          </header>
                          <div class="panel-body">
                             
                              <form class="form-horizontal "  onsubmit="return false;">
                                <div class="form-group"  style="" >
                                      
                                      <div class="col-lg-10">
                                          <button onClick="backDownloadreport()" class="btn btn-primary" >Back </button>

                                      </div>
                                  </div>
                                   <div style="" class="row">
                                    <div class="col-sm-2">
          <button onClick="showHighDownloadsCharts()" class="btn btn-success" >Line Graph </button>
        </div>
        <div class="col-sm-2">
          <button onClick="downloadsPieChart_btter()" class="btn btn-success" >Pie Chart </button>
        </div>
         <div class="col-sm-2">
          <button onClick="downloadsDonutChart()" class="btn btn-success" >Donought Chart </button>
        </div>
              
                                   </div>
                                  <div class="form-group">
                                    <div id="container11" style="margin: 60px 0;">
                                      
                                    </div>
                                     <div id="containerPie" style="margin: 60px 0;">
                                      
                                    </div>
                                    
                                  </div>
                                  
                              </form>
                            
                          </div>
                           
                      </section>
                     
                     
                     
                  </div>

<!-- end of graph -->

        <div class="col-lg-12" id="graphDaillyreportFunctions">
                      <section class="panel">
                          <header class="panel-heading">
                             Filter/Search by :
                          </header>
                          <div class="panel-body">
                              <form class="form-horizontal "  onsubmit="return false;">
                                <div class="form-group" style="display: none;">
                                      <label class="control-label col-lg-2" for="inputSuccess">Inline checkboxes</label>
                                      <div class="col-lg-10">
                                          <label class="checkbox-inline">
                                              <input type="checkbox" id="inlineCheckbox1" value="option1"> 1
                                          </label>
                                          <label class="checkbox-inline">
                                              <input type="checkbox" id="inlineCheckbox2" value="option2"> 2
                                          </label>
                                          <label class="checkbox-inline">
                                              <input type="checkbox" id="inlineCheckbox3" value="option3"> 3
                                          </label>

                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="control-label col-lg-2" for="inputSuccess">Search :</label>
                                      <div class="col-lg-10">
                                          <div class="row">
                                              <div style="display: none;" class="col-lg-2">
                                                  <input type="text" class="form-control" placeholder=".col-lg-2">
                                              </div>
                                              <div style="display: none;" class="col-lg-3">
                                                <label>Search by <label>
                                                  <input type="text" id="pageSearhPageName" class="form-control" placeholder="by Page Name">
                                              </div>
                                              <div class="col-lg-6" >
                                                <label>  <label>
                                                  <input style="width: 430px;" id="downloadSearch"  type="text" class="form-control" placeholder="Module Code , RegNumber">
                                              </div>
                                              <div  class="col-lg-2" style="display: none;">
                                                <input type="submit" class="btn btn-primary" id="bntSearchDailyReport"  value="Search">
                                              </div>
                                              <div  class="col-lg-2" >
                                                <input type="reset" class="btn "  value="Reset">
                                              </div>
                                          </div>

                                      </div>
                                  </div>
                                   <div class="form-group" id="actionDailyReports" style="display: none;">
                                      <label  class="control-label col-lg-2" for="inputSuccess">Action</label>
                                      <div class="col-lg-10">
                                          <label class="checkbox-inline">
                                              <input type="submit" style="display: none;" onClick ="excelfyDownload()" class="btn btn-success" id="inlineCheckbox1" value="Export to Excel"> 
                                          </label>
                                          <label class="checkbox-inline">
                                             <input type="submit" id="" onClick ="csvfyDownload()" class="btn btn-success" value="Export to CSV File"> 
                                          </label>
                                          <label class="checkbox-inline">
                                              <input type="submit" id="" onClick="printDailyReport()" class="btn btn-success" value="Print Table"> 
                                          </label>
                                          <label class="checkbox-inline">
                                              <input type="submit" id="" onClick="showDownloadGraphs()" class="btn btn-success" value="View Graph"> 
                                          </label>

                                      </div>

                                  </div>
                                  <div class="col-lg-10" id="downloadDiv" style="display: none;">

                                  </div>
                                  <div class="col-lg-10" id="container222" style="margin: 20px 0; display: none;">
                                      
                                    </div>
                                    <div class="col-lg-10" id="container333" style="margin: 20px 0;display: none;">
                                      
                                    </div>

                                  
                              </form>
                          </div>
                      </section>
                     
                     
                     
                  </div>

      </div>
       <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Download Reports 
                          </header>
                          <div id="content" class="table-responsive">
                            <table id="tableID" class="table">
                              <thead>
                                <tr>                                  
                                  <th>Module Code</th>
                                  <th>Student</th>
                                  <th>Download Time</th>
                                  <th>Download Date </th>
                                  <th>Download Count</th>
                                  <th>Browsers used</th>                                  
                                </tr>
                              </thead>
                              <tbody id="showDailyReport" >

                               
                              </tbody>
                            </table>
                          </div>

                      </section>
                  </div>
              </div>

                    
                   
                <!-- statics end -->
              
            
        


          </section>
         
      </section>
      <!--main content end-->
  </section>
  <!-- container section start -->

    <!-- javascripts -->
    <script src="js/jquery.js"></script>
  <script src="js/jquery-ui-1.10.4.min.js"></script>
    <script src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <!-- bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>   
    <!--custome script for all page-->
    <script src="js/scripts.js"></script>
    <!-- custom script for this page-->
    
  <script src="js/jquery.autosize.min.js"></script>
  <script src="js/jquery.placeholder.min.js"></script>
  <script src="js/gdp-data.js"></script>  
  <script src="js/morris.min.js"></script>
  
  <script src="js/jquery.slimscroll.min.js"></script>
 <!-- cutsome use -->
 <script type="text/javascript" src="js/bootstrap-dialog.min.js"></script>
   <script type="text/javascript" src="js/loadingoverlay.min.js"></script>
   <script type="text/javascript" src="js/loadingoverlay_progress.min.js"></script>

   <script src="js/highcharts.js"></script>
<script src="js/series-label.js"></script>
<script src="js/exporting.js"></script>

   <script src="js/customeQuery.js"></script> 


  

   <script type="text/javascript">
    function pdfy(){
     
    }
    


     getDaillyReportDownloads("all","level-all" , "all");
   </script>

  </body>
</html>
