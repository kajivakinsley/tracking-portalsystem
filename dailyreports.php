<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">
<?php $PAGE = 'dailyreports'; ?>
    <title> Admin Daily Reports page </title>

    <!-- Bootstrap CSS -->    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="css/elegant-icons-style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />    
    <!-- Custom styles -->
  <link rel="stylesheet" href="css/fullcalendar.css">
  <link href="css/widgets.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
  
  <link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap-dialog.min.css">
    
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
     
      
    
      <!--header end-->
 <?php  include 'header.php' ; ?>   
      <!--sidebar start-->
    <?php  include 'sidebar.php' ; ?>   
      <!--sidebar end-->
      
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">            
              <!--overview start-->
        <div class="row">
        <div class="col-lg-12">
          
          <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="admin_index.php">Home</a></li>
            <li><i class="fa fa-home"></i><a href="dailyreports.php">Daily Reports</a></li>
            
          </ol>
        </div>
      </div>
      
      <!-- Today status end -->              
        
      <div class="row">

<!-- start of graph -->
<div class="col-lg-12" id="showgraphsDaillyReports" style="display: none;">
                      <section class="panel">
                          <header class="panel-heading">
                           Showing Graph :
                          </header>
                          <div class="panel-body">
                             
                              <form class="form-horizontal "  onsubmit="return false;">
                                <div class="form-group"  style="" >
                                      
                                     
                                  </div>
                                  <div style="" class="row">
                                     <div class="col-sm-2">
                                          <button onClick="backdailyreport()" class="btn btn-primary" >Back </button>

                                      </div>
        <div class="col-sm-2">
           <button onClick="dailyReportLineGrapgh()" class="btn btn-success" >Line Graph </button>
        </div>
        <div class="col-sm-2">
          <button onClick="dailyPieChart()" class="btn btn-success" >Pie Chart </button>
        </div>
         <div class="col-sm-2">
          <button onClick="dailyDonutChart()" class="btn btn-success" >Donought Chart </button>
        </div>
    </div>
                                  <div class="form-group">
                                    <div id="container11" style="margin: 20px 0;">
                                      
                                    </div>
                                    <div id="container222" style="margin: 20px 0;display: none;">
                                      
                                    </div>
                                    <div id="container333" style="margin: 20px 0;display: none;">
                                      
                                    </div>
                                    
                                  </div>
                                  
                              </form>
                            
                          </div>
                           
                      </section>
                     
                     
                     
                  </div>

<!-- end of graph -->

        <div class="col-lg-12" id="graphDaillyreportFunctions">
                      <section class="panel">
                          <header class="panel-heading">
                             Filter/Search by :
                          </header>
                          <div class="panel-body">
                              <form class="form-horizontal "  onsubmit="return false;">
                                <div class="form-group" style="display: none;">
                                      <label class="control-label col-lg-2" for="inputSuccess">Inline checkboxes</label>
                                      <div class="col-lg-10">
                                          <label class="checkbox-inline">
                                              <input type="checkbox" id="inlineCheckbox1" value="option1"> 1
                                          </label>
                                          <label class="checkbox-inline">
                                              <input type="checkbox" id="inlineCheckbox2" value="option2"> 2
                                          </label>
                                          <label class="checkbox-inline">
                                              <input type="checkbox" id="inlineCheckbox3" value="option3"> 3
                                          </label>

                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="control-label col-lg-2" for="inputSuccess">Search :</label>
                                      <div class="col-lg-10">
                                          <div class="row">
                                              <div style="display: none;" class="col-lg-2">
                                                  <input type="text" class="form-control" placeholder=".col-lg-2">
                                              </div>
                                              <div style="display: none;" class="col-lg-3">
                                                <label>Search by <label>
                                                  <input type="text" id="pageSearhPageName" class="form-control" placeholder="by Page Name">
                                              </div>
                                              <div class="col-lg-6" >
                                                <label>  <label>
                                                  <input style="width: 430px;" id="regNumberSearch"  type="text" class="form-control" placeholder="Regnumber , by Page Name.....">
                                              </div>
                                              <div  class="col-lg-2" style="display: none;">
                                                <input type="submit" class="btn btn-primary" id="bntSearchDailyReport"  value="Search">
                                              </div>
                                              <div  class="col-lg-2" >
                                                <input type="reset" class="btn "  value="Reset">
                                              </div>
                                          </div>

                                      </div>
                                  </div>
                                  <div class="form-group" id="" style="">
                                      <label  class="control-label col-lg-2" for="inputSuccess">Filter By :</label>
                                       <div class="col-lg-4" >
                                                   <select id="filderByDailyLogs" class="form-control m-bot15">
                                                    <option value="null"></option>
                                                    <option value="page">Activity</option>
                                                    <option value="time_visit">Log Time</option>
                                                    <option value="date_visited">Log Date</option>
                                                    <option value="act_std">Activity and Student </option>
                                                   
                                                  </select>
                                              </div>
                                              <div  class="col-lg-6" >
                                                <input type="reset" class="btn btn-info " onClick="getFilterdata()"  value="Filter">
                                              </div>
                                      <div class="col-lg-10">
                                          
 
                                      </div>
 
                                  </div>
                                   <div class="form-group" id="actionDailyReports" style="display: none;">
                                      <label  class="control-label col-lg-2" for="inputSuccess">Action</label>
                                      <div class="col-lg-10">
                                          <label class="checkbox-inline">
                                              <input type="submit" style="display: none;" onClick ="excelfy()" class="btn btn-success" id="inlineCheckbox1" value="Export to Excel"> 
                                          </label>
                                          <label class="checkbox-inline">
                                             <input type="submit" id="" onClick ="csvfy()" class="btn btn-success" value="Export to CSV File"> 
                                          </label>
                                          <label class="checkbox-inline">
                                              <input type="submit" id="" onClick="printDailyReport()" class="btn btn-success" value="Print Table"> 
                                          </label>
                                          <label class="checkbox-inline">
                                              <input type="submit" id="" onClick="showDailyReportGraphs()" class="btn btn-success" value="View Graph"> 
                                          </label>

                                      </div>

                                  </div>
                                  <div class="col-lg-10" id="downloadDiv" style="display: none;">

                                  </div>

                                  
                              </form>
                          </div>
                      </section>
                     
                     
                     
                  </div>

      </div>
       <div class="row">
        <div class="col-sm-6" id="showfilterResultsTable" style="display: none;">
                      <section class="panel">
                          <header class="panel-heading">
                              Filter Result &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a onClick="closeFilter()" href="javascript:void(0)">Close </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a onClick="filterResultBarChart()" href="javascript:void(0)">BarGraph </a>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a onClick="filterResultPieChart()" href="javascript:void(0)">Pie Graph </a>
                          </header>
                          <div id="contentTableFilter " class="table-responsive">
                            <div id="qweqweqw" class="container-fluid"></div>
                           
                          </div>
 
                      </section>
                  </div>
                  <div class="col-lg-6" id="divfilterResultBarChart">
                      <section class="panel">
                          <div class="panel-body">
                            <div id="dailyReportfilterResultBarChart"></div>
                            <div id="dailyReportfilterResultPieChart"></div>
                          </div>
                      </section>
                  </div>
                  <hr>
                  <div class="col-lg-10">
                      <section class="panel">
                          <header class="panel-heading">
                              Reports 
                          </header>
                          <div id="content" class="table-responsive">
                            <table id="tableID" class="table">
                              <thead>
                                <tr>                                  
                                  <th>Page</th>
                                  <th>Student</th>
                                  <th>Log Time</th>
                                  <th>Log Date </th>
                                  <th>Visit Counts</th>
                                  <th>Ip Address</th>                                  
                                </tr>
                              </thead>
                              <tbody id="showDailyReport" >

                               
                              </tbody>
                            </table>
                          </div>

                      </section>
                  </div>
              </div>

                    
                   
                <!-- statics end -->
              
            
        


          </section>
         
      </section>
      <!--main content end-->
  </section>
  <!-- container section start -->

    <!-- javascripts -->
    <script src="js/jquery.js"></script>
  <script src="js/jquery-ui-1.10.4.min.js"></script>
    <script src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <!-- bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>   
    <!--custome script for all page-->
    <script src="js/scripts.js"></script>
    <!-- custom script for this page-->
    
  <script src="js/jquery.autosize.min.js"></script>
  <script src="js/jquery.placeholder.min.js"></script>
  <script src="js/gdp-data.js"></script>  
  <script src="js/morris.min.js"></script>
  
  <script src="js/jquery.slimscroll.min.js"></script>
 <!-- cutsome use -->
 <script type="text/javascript" src="js/bootstrap-dialog.min.js"></script>
   <script type="text/javascript" src="js/loadingoverlay.min.js"></script>
   <script type="text/javascript" src="js/loadingoverlay_progress.min.js"></script>


   <script src="js/customeQuery.js"></script> 
   <script src="js/highcharts.js"></script>
<script src="js/series-label.js"></script>
<script src="js/exporting.js"></script>
  

   <script type="text/javascript">
    function pdfy(){
     
    }
    


    getDaillyReport("all","level-all" , "all");
let filterBarGraphsOption = {
                    chart: {
                        renderTo: 'dailyReportfilterResultBarChart',
                        type: 'bar'
                    },
                    title: {
                        text: 'Daily Reports Statistics',
                        x: -20 //center
                    },
                    subtitle: {
                        text: 'Cut @ 2017',
                        x: -20
                    },
                    xAxis: {
                        categories: []
                    },
                    yAxis: {
                        title: {
                            text: 'Report Counts'
                        },
                        plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }]
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>:<b>{point.y}</b> of total<br/>'
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y}'
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 100,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                        shadow: true
                    },
                    series: []
                };
function filterResultBarChart(){
  let _search = $("#filderByDailyLogs").val();
  _search   = _search == ''? 'all':_search;
  edistSeachVal = _search;

  $.post("slave.php",{
    postFilterdailyReportBargraph  :_search

  },function(response_){    

  
    var response = $.parseJSON(response_);
    filterBarGraphsOption.xAxis.categories = response[0]['data'];
      filterBarGraphsOption.series[0] = response[1];
      chart = new Highcharts.Chart(filterBarGraphsOption);
      
      $("#dailyReportfilterResultBarChart").effect( "bounce", {times:5}, 700 , function(){
            $("#dailyReportfilterResultBarChart").slideDown("slow");    
      });
      
      

  });

}
var filterPieChartOption = {
                    chart: {
                        renderTo: 'dailyReportfilterResultPieChart',
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: 'Daily Reports Statistics'
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>' + this.point.name + '</b>: ' + this.y+ ' ';
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                formatter: function() {
                                    return '<b>' + this.point.name + '</b>: ' + this.y;
                                }
                            },
                            showInLegend: true
                        }
                    },
                    series: []
                };
function filterResultPieChart(){

  let _search = $("#filderByDailyLogs").val();
  _search   = _search == ''? 'all':_search;
  edistSeachVal = _search;

  $.post("slave.php",{
    postFilterdailyReportPiegraph :_search

  },function(response_){    
    
                
    var response = $.parseJSON(response_);
     filterPieChartOption.series = response;
      chart = new Highcharts.Chart(filterPieChartOption);
      

      $("#dailyReportfilterResultPieChart").effect( "bounce", {times:5}, 800 , function(){
           $("#dailyReportfilterResultPieChart").slideDown("slow"); 
      });
      

  });

}

   </script>

  </body>
</html>