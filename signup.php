<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="shortcut icon" href="img/favicon.png">
<?php $PAGE = 'signgup'; ?>
    <title>Sign up</title>

    <!-- Bootstrap CSS -->    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="css/elegant-icons-style.css" rel="stylesheet" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    <!-- Custom dialog overlay for bootstrap -->
    <link rel="stylesheet" href="css/bootstrap-dialog.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/css/bootstrap-dialog.min.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->
<style type="text/css">
/*bootstrap class override for login page only*/
.form-control{
    border-radius: 0px;
    margin: 12px 3px;
    height: 40px;
}
.logo{
    margin: auto;
    text-align: center;
    padding-top: 20%;
}
.logo img{
    height: 70px;
}
/*footer*/ 
.footer a{
    color: #000;
    text-decoration: none;
}
.footer{
    color: #000;
    text-align: center;
}
/*footer end*/ 


/*for logintemplate blue*/
.grayBody{
    background-color: #E9E9E9;
}
.loginbox{
    margin-top: 5%;
    border-top: 6px solid #0088D8;
    background-color:#fff;
    padding: 20px;
    box-shadow: 0 10px 10px -2px rgba(0,0,0,0.12),0 -2px 10px -2px rgba(0,0,0,0.12);    
}
.singtext{    
    font-size: 21px;  
    color: #0088D8;
    font-weight: 500;
    letter-spacing: 1px;
}
.submitButton{
    background-color: #0088D8;
    color: #fff;
    margin-top: 12px;
    margin-bottom: 10px;
    padding: 10px 0px;
    width: 100%;
    margin-left: 2px;
    font-size: 16px;
    border-radius: 0px;
    outline: none;
}
.submitButton:hover,.submitButton:focus{
    color: #fff;  
    outline: none;
}
.forGotPassword{
    background-color: #F2F2F2; 
    padding: 15px 0px;
    text-align: center;

}
.forGotPassword:hover{
   box-shadow: 0 10px 10px -2px rgba(0,0,0,0.12);    
}
.forGotPassword a{
    color: #000;
    outline: none;
}
.forGotPassword a:hover, .forGotPassword a:active,.forGotPassword a:focus{  
    text-decoration: none; 
    outline: none;
}
</style>
</head>

<body  style="background-color: #337ab7;">

 <div class="container" >  
    <div class="col-lg-4 col-md-3 col-sm-2"></div>
    <div class="col-lg-6 col-md-6 col-sm-8">
        <div class="logo">
            <img id="logoSign" src="img/User -1.png"  alt="Logo"  > 
        </div>
        <div class="row loginbox">                    
            <div class="col-lg-12">
                <span class="singtext" id="status">Sign Up </span>   
            </div>
            <div class="col-lg-12  col-md-12 col-sm-12">
              <select id="createAccountType" class="selectpicker form-control" data-live-search="true" onchange="iconChange();">
                  <option value= "null">Select User</option>
                  <option value= "student">Student</option>
                  <option value= "lecturer">Lecturer</option>
                  <option value= "admin">Administrator</option>


              </select>
              <div id="stdElements" style="display: none;">
              <div class="col-lg-12  col-md-12 col-sm-12">
               <select id="stdlevel" class="selectpicker form-control" data-live-search="true" >
                  <option value= "null">Select Level</option>
                  <option value="1.1">Level 1.1</option>
                  <option  value="1.2">Level 1.2</option>
                  <option value="2.1">Level 2.1</option>
                  <option  value="2.2">Level 2.2</option>
                  <option  value="3.1">Level 3.1</option>
                  <option  value="3.2">Level 3.2</option>
                  <option  value="4.1">Level 4.1</option>
                  <option  value="4.2">Level 4.2</option>

              </select>
          </div>
          <div class="col-lg-12  col-md-12 col-sm-12">
           <select id="stdprograms" class="selectpicker form-control" data-live-search="true" >
              <option value= "null">Select Program</option>
              <?php 
              require 'dbconx.php';              
              $sq= "SELECT DISTINCT `dept`,`gname` FROM `programs` ";
              $q = mysqli_query($con , $sq);
              while($rw = mysqli_fetch_assoc($q)){
                ?>
                <option value="<?php echo $rw['gname']; ?>" ><?php echo $rw['gname'] .' - '.$rw['dept']; ?></option>
                <?php
              }
              //include_once 'schoolslist.php'; ?>

          </select>
      </div>
  </div>
      <div class="row">
        <div class="col-xs-6 col-md-6">
          <label>First Name</label>
          <input type="text" name="firstname" id="firstname" class="form-control input-lg" placeholder="First Name"  />                        </div>
          <div class="col-xs-6 col-md-6">
            <label>Last Name</label>
            <input type="text" name="lastname" id="lastname" value="" class="form-control input-lg" placeholder="Last Name"  />                        </div>
        </div>
        <label>Registration Number</label>

        <div class="col-lg-12 col-md-12 col-sm-12">
            <input class="form-control" type="text" id="regNumber" placeholder="Registration Number..." autofocus > 
        </div>
        <div class="row">
            <div class="col-xs-6 col-md-6">
               <label>Password</label>
               <input class="form-control" type="password" id="password" placeholder="Password">
           </div>
           <div class="col-xs-6 col-md-6">
              <label>Confirm Password</label>
              <input class="form-control" type="password" id="conpassword" placeholder="Confirm Password">
          </div>
      </div>
      <div class="row">
       <div class="col-xs-6 col-md-6">

        <a href="#" class="btn  submitButton" id="createAccount" type="submit"> Create Account </a> 
    </div>    
    <div class="col-xs-6 col-md-6">
        <a href="login.php" class="btn  submitButton">Log in </a> 
    </div> 
</div>                 

</div>

<br>                
<br>
<footer  class="footer">                
    <p> E- Portal System</p>
    <p >©2017   Developed in 2017 for final year project (C.U.T)</p>                 
</footer> <!--footer Section ends-->

</div>
<div class="col-lg-4 col-md-3 col-sm-2"></div>
</div>
   

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>  
    <script type="text/javascript" src="js/bootstrap-dialog.min.js"></script>
    <script type="text/javascript" src="js/loadingoverlay.min.js"></script>
    <script type="text/javascript" src="js/loadingoverlay_progress.min.js"></script>
    <script src="js/customeQuery.js"></script> 

</body>





</html>
