<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">
<?php $PAGE = 'upload file Modules'; ?>
    <title> Upload Files to Modules | Portal page </title>

    <!-- Bootstrap CSS -->    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="css/elegant-icons-style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />    
    <!-- Custom styles -->
  <link rel="stylesheet" href="css/fullcalendar.css">
  <link href="css/widgets.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
  
  <link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap-dialog.min.css">
    
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
     
      
    
      <!--header end-->
 <?php  include 'header.php' ; ?>   
      <!--sidebar start-->
    <?php  include 'sidebar.php' ; ?>   
      <!--sidebar end-->
      
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">            
              <!--overview start-->
        <div class="row">
        <div class="col-lg-12">
          
          <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
            <li><i class="fa fa-laptop"></i><a href="uploadfileModules.php">Upload Module Files eg pdf,doxc,txt file types</a></li>               
          </ol>
        </div>
      </div>
      
      <!-- Today status end -->              
        
      <div class="row">
       <div class="col-lg-6">
        <section class="panel">
          <header class="panel-heading">
             <i class="icon_document_alt"></i>
            Upload Files or document to students  
          </header>
          <div class="panel-body">
            <form class="form-horizontal" id="fomFileUpload" role="form" onsubmit="return false;" autocomplete="off">
              <div class="form-group">
                <label class="control-label col-lg-2" for="inputSuccess">Programs</label>
                <div class="col-lg-10">
                  <select class="form-control m-bot15" id="programSelect">
                     <option value= "null">Select Program</option>
                    <?php 
                    require 'dbconx.php';
                    $who = mysqli_fetch_assoc(mysqli_query($con , "SELECT program , registration_number FROM users_lectures WHERE registration_number = '$_SESSION[user]'  "));
                    $sq = mysqli_query($con , "SELECT DISTINCT * FROM `programs` WHERE gname = '$who[program]' ");
                    $optionView = "";
                    while($qw = mysqli_fetch_assoc($sq)){
                        for($c=0 ; $c<12 ; $c++){
                          $f="course" . $c;
                            if(!empty($qw[$f]) && $qw[$f] !='NULL'){
                             $optionView .="<option value = '$qw[$f]' >". $qw[$f] ."</option>";
                           }
                       }
                     } 
                     echo  $optionView;                   

                     ?>
                   
                    
                  </select>


                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail1" class="col-lg-2 control-label">Name/Title</label>
                <div class="col-lg-10">
                  <input type="text"  class="form-control" id="docName" placeholder="Title ... ">
                  <p class="help-block">e.g Introduction Notes , CUIT 101</p>
                </div>
              </div>
              <div class="form-group">
                <label for="file" class="col-lg-2 control-label">File/Document</label>
                <div class="col-lg-10">
                  <input type="file" accept=".doc,.pdf,.txt" class="form-control" id="lecturerfile" >
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" id="uploadedCheckBoxResetForm"> Reset Form After Upload
                    </label>
                  </div>
                </div>
              </div>
               <div class="row">
                
             
                <div class="col-xs-6 col-md-6">
                  <button type="submit" id="lecturerUploadFileDoc" class="btn btn-primary">Upload Doc</button>
                </div>
              
             
                 <div class="col-xs-6 col-md-6">
                  <button type="reset" class="btn ">Reset</button>
                </div>
             
              
              </div>
            </form>
          </div>
        </section>

      </div>
      <div class="col-lg-6">
                      <section class="panel">
                          <header class="panel-heading">
                             Files you have uploaded so far
                          </header>
                          
                         <div id="showFilesuploaded" class="list-group" >
                                                         
                              
                             </div>
                      </section>
                  </div>

    </div>

                    
                   
                <!-- statics end -->
              
            
        


          </section>
         
      </section>
      <!--main content end-->
  </section>
  <!-- container section start -->

    <!-- javascripts -->
    <script src="js/jquery.js"></script>
  <script src="js/jquery-ui-1.10.4.min.js"></script>
    <script src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <!-- bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>   
    <!--custome script for all page-->
    <script src="js/scripts.js"></script>
    <!-- custom script for this page-->
    
  <script src="js/jquery.autosize.min.js"></script>
  <script src="js/jquery.placeholder.min.js"></script>
  <script src="js/gdp-data.js"></script>  
  <script src="js/morris.min.js"></script>
  
  <script src="js/jquery.slimscroll.min.js"></script>
 <!-- cutsome use -->
 <script type="text/javascript" src="js/bootstrap-dialog.min.js"></script>
   <script type="text/javascript" src="js/loadingoverlay.min.js"></script>
   <script type="text/javascript" src="js/loadingoverlay_progress.min.js"></script>
   <script src="js/customeQuery.js"></script> 
   <script type="text/javascript">
    $(function() {
        pagevisitLecturer();
     });
     getAllLectureUploads( "<?php echo  $_SESSION['user'];?> ", 'all');
   </script>

  </body>
</html>
