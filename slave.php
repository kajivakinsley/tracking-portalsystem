<?php

/****************************************************************************/

// SELECT * FROM `tracking_student_page`  WHERE  `date_visited` between '2017-10-01' and '2017-10-07'
// 
// 
// WHERE DATE(date_time) < CURDATE()
// 
// To show results of 10 days
// WHERE date_time < NOW() - INTERVAL 10 DAY
// 
// To show results before 10 days
// WHERE DATE(date_time) < DATE(NOW() - INTERVAL 10 DAY)





/****************************************************************************/



/****************************************************************************/

if(isset($_POST['postFilterdailyReportPiegraph'])){

	require 'dbconx.php';	
	

	$sq  = mysqli_query($con , "SELECT `page`, count(*) as 'count'  , id FROM tracking_student_page GROUP BY `page`  ORDER BY `id` DESC ");
	// SELECT `moduleName`, count(*) as 'count'  FROM tracking_student_page GROUP BY `moduleName`

	if(mysqli_num_rows($sq)<1){
			echo "empty";exit;
		}
	
	$count_bln = $results = $rw  = array();
	$count_bln['type'] = 'pie';
	while($row = mysqli_fetch_array($sq)){		
		$count_bln ['data'][] = array( $row['page'] , (int) $row['count']   );

	}
	array_push($results , $count_bln );
	echo json_encode($results , JSON_NUMERIC_CHECK);
	exit;
	

}



/****************************************************************************/

if(isset($_POST['postFilterdailyReportBargraph'])){

	require 'dbconx.php';	
	

	 $sq  = mysqli_query($con , "SELECT *  FROM  tracking_student_page  ORDER BY `id` DESC ");
	/*$sq  = mysqli_query($con , "SELECT `page`, count(*) as 'count'  , id FROM tracking_student_page GROUP BY `moduleName`  ORDER BY `id` DESC ");*/

	if(mysqli_num_rows($sq)<1){
			echo "empty";exit;
		}
		$count_bln = $results = $rw  = array();

	while($row = mysqli_fetch_array($sq)){		
		$count_bln ['data'][] = $row['page'] ;
		$rw        ['data'][] = $row['visit_count'] ;
	}
	array_push($results , $count_bln );
	array_push($results , $rw );
	echo json_encode($results , JSON_NUMERIC_CHECK);
	exit;
	

}

/****************************************************************************/

if(isset($_POST['postFilterfileuploadLecturereHighChartsPie2'])){
	$search  = $_POST['postFilterfileuploadLecturereHighChartsPie2'];
	require"dbconx.php";
	
	$sql = mysqli_query($con , $search == 'all' ?
		"SELECT `module_code`, count(*) as 'count' ,`lecturer` FROM reading_materials GROUP BY `module_code` " 
		:
		 "SELECT `module_code`, count(*) as 'count' ,`lecturer` FROM reading_materials WHERE lecturer LIKE '%$search%' OR module_code LIKE '%$search%'  GROUP BY `module_code` " );

	if(mysqli_num_rows($sql)<1){
		echo "empty";
		exit;
	}
	$count_bln = $results = $rw  = array();
	
	$count_bln['type'] = 'pie';
	while($row = mysqli_fetch_array($sql)){		
		$count_bln ['data'][] = array( $row['module_code'] , (int) $row['count']   );

	}
	array_push($results , $count_bln );

	echo json_encode($results , JSON_NUMERIC_CHECK);
	exit;

}


/****************************************************************************/

if(isset($_POST['postshow_fileuploadLecturerGraphs'])){
	$search  = $_POST['postshow_fileuploadLecturerGraphs'];
	require"dbconx.php";
	
	$sql = mysqli_query($con , $search == 'all' ?
		"SELECT `module_code`, count(*) as 'count' ,`lecturer` FROM reading_materials GROUP BY `module_code` " 
		:
		 "SELECT `module_code`, count(*) as 'count' ,`lecturer` FROM reading_materials WHERE lecturer LIKE '%$search%' OR module_code LIKE '%$search%'  GROUP BY `module_code` " );
	
if(mysqli_num_rows($sql)<1){
		echo "empty";
		exit;
	}
	$count_bln = $results = $rw  = array();

	while($row = mysqli_fetch_array($sql)){		
		$count_bln ['data'][] = $row['module_code'] ;
		$rw        ['data'][] = $row['count'] ;	

	}
	array_push($results , $count_bln );
	array_push($results , $rw );
	echo json_encode($results , JSON_NUMERIC_CHECK);
	exit;

}


/****************************************************************************/




if(isset($_POST['postshow_fileuploadLecturer'])){
	$search  = $_POST['postshow_fileuploadLecturer'];

	require"dbconx.php";

	$sql = mysqli_query($con , $search == 'all' ?
		"SELECT `module_code`, count(*) as 'count' ,`lecturer` FROM reading_materials GROUP BY `module_code` " 
		:
		 "SELECT `module_code`, count(*) as 'count' ,`lecturer` FROM reading_materials WHERE lecturer LIKE '%$search%' OR module_code LIKE '%$search%'  GROUP BY `module_code` " );
	if(mysqli_num_rows($sql)<1){
		echo "empty";
		exit;
	}

	while( $we = mysqli_fetch_assoc($sql) ){
		?>
		<tr>
			<th></th>                                  
			<td >   <?php   echo $we['module_code'];   ?>   </td>
			<td >   <?php   echo $we['lecturer'] ;    ?>   </td>                              
			<td >   <?php   echo $we['count'] ;       ?>   </td>  
			<th></th>                                                       

		</tr>

		<?php

	}



}





/****************************************************************************/





if(isset($_POST['postFilterRequestDownloadHighChartsPie2'])){
	$search  = $_POST['postFilterRequestDownloadHighChartsPie2'];
	require 'dbconx.php';
		
	$sq  = mysqli_query($con ,$search == 'all' ?
	 "SELECT `moduleName`, count(*) as 'count'  FROM tracking_student_downloads GROUP BY `moduleName`"
	 : 
	  "SELECT `moduleName`, count(*) as 'count'  FROM tracking_student_downloads WHERE student LIKE '%$search%' OR  moduleName LIKE '%$search%' GROUP BY `moduleName` "
	);
	

	$count_bln = $results = $rw  = array();
	$count_bln['type'] = 'pie';
	while($row = mysqli_fetch_array($sq)){		
		$count_bln ['data'][] = array( $row['moduleName'] , (int) $row['count']   );

	}
	array_push($results , $count_bln );
	echo json_encode($results , JSON_NUMERIC_CHECK);
	exit;
}


/****************************************************************************/




/****************************************************************************/

if(isset($_POST['postFilterRequestDownloadHighCharts2'])){
	$search  = $_POST['postFilterRequestDownloadHighCharts2'];
	require 'dbconx.php';
		
	$sq  = mysqli_query($con ,$search == 'all' ?
	 "SELECT `moduleName`, count(*) as 'count'  FROM tracking_student_downloads GROUP BY `moduleName`"
	 : 
	  "SELECT `moduleName`, count(*) as 'count'  FROM tracking_student_downloads WHERE student LIKE '%$search%' OR  moduleName LIKE '%$search%' GROUP BY `moduleName` "
	);
	

	$count_bln = $results = $rw  = array();

	while($row = mysqli_fetch_array($sq)){		
		$count_bln ['data'][] = $row['moduleName'] ;
		$rw        ['data'][] = $row['count'] ;	

	}
	array_push($results , $count_bln );
	array_push($results , $rw );

	echo json_encode($results , JSON_NUMERIC_CHECK);
	exit;
}


/****************************************************************************/

if(isset($_POST['poststdDelete'])){
	require 'dbconx.php';
	$id = $_POST['poststdDelete'];
	if(mysqli_query($con , "DELETE FROM `users_students` WHERE registration_number = '$id' ")){
		echo "done";exit;	
	}else{
		echo "failed";exit;
	}
}



/****************************************************************************/


if(isset($_POST['postupDateFile'])){

	$name = $_POST['post_name'];
	$surname = $_POST['post_surname'];
	$password = $_POST['post_password'];
	$level = $_POST['post_level'];
	$regnumber = $_POST['postregnumber'];
	require 'dbconx.php';

	if(mysqli_query($con ,"UPDATE users_students SET name= '$name' , surname='$surname' , password='$password' , level = '$level'  WHERE registration_number= '$regnumber' ")){
		echo "done";exit;
	}else{
		echo "failed";exit;
	}


}



/****************************************************************************/

if(isset($_POST['postsearch'])){
	$seach = $_POST['postsearch'];
	require 'dbconx.php';

	if($seach == 'all'){
		$sq = mysqli_query($con , "SELECT * FROM users_students  " );
	}else{
		$sq = mysqli_query($con , "SELECT * FROM users_students WHERE  registration_number LIKE '%$allAttributs%' " );
	}
	if(mysqli_num_rows($sq)<1){
		echo "empty";exit;
	}
	while($rw = mysqli_fetch_assoc($sq)){
		?>
		<tr >

			<td ><?php echo $rw['registration_number']; ?></td>
			<td >
				<a href="javascript:void(0)" onclick="edituser(
						'<?php echo $rw['registration_number'] ; ?>',
						'<?php echo $rw['surname'] ; ?>',
						'<?php echo $rw['name'] ; ?>',
						'<?php echo $rw['password'] ; ?>',
						'<?php echo $rw['date_created'] ; ?>',
						'<?php echo $rw['level'] ; ?>',
						'<?php echo $rw['program'] ; ?>'


				)">Edit</a> <br>
				<a href="javascript:void(0)" onclick="deletuser(
				'<?php echo $rw['registration_number'] ; ?>'
				);" >Delete</a> </td>

			</tr>
	<?php	
	}


}


/****************************************************************************/


if(isset($_POST['postDaillyReportxel'])){

	$byBy 		= $_POST['postDaillyReportxelbyBy'];
	$dateFilter = $_POST['postDaillyReportxeldateFilter'];
	$allAttributs= $_POST['postDaillyReportxelallAttributs'];

	require 'dbconx.php';

	if($allAttributs == 'all'){
		$sq  = mysqli_query($con , "SELECT * FROM  tracking_student_page ORDER BY `id` DESC ");	
	}else{
		$sq  = mysqli_query($con , "SELECT * FROM  tracking_student_page WHERE student LIKE '%$allAttributs%' ORDER BY `id` DESC ");	
	}

	if(mysqli_num_rows($sq)<1){
		echo "empty";exit;
	}


$export = $sq;

$fields = 8;

 $headers=mysqli_fetch_fields($sq );
    
    foreach($headers as $header){
    //	$header .= $header->name;
    }

 $data = '';
while( $row = mysqli_fetch_row( $export ) )
{
    $line = '';
    foreach( $row as $value )
    {                                            
        if ( ( !isset( $value ) ) || ( $value == "" ) )
        {
            $value = "\t";
        }
        else
        {
            $value = str_replace( '"' , '""' , $value );
            $value = '"' . $value . '"' . "\t";
        }
        $line .= $value;
    }
    $data .= trim( $line ) . "\n";
}
$data = str_replace( "\r" , "" , $data );

if ( $data == "" ){
    $data = "\n(0) Records Found!\n";                        
}

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=your_desired_name.xls");
header("Pragma: no-cache");
header("Expires: 0");
//print "$header\n$data";
}




/****************************************************************************/


if(isset($_POST['GlobalDownloadCSV'])){
	downloadFile($_POST['GlobalDownloadCSV']);
}

/**********************************************************************/
if(isset($_POST['postDaillyReportDowloadcsv'])){

	$byBy 		= $_POST['postDaillyReportDowloadcsvbyBy'];
	$dateFilter = $_POST['postDaillyReportDowloadcsvdateFilter'];
	$allAttributs= $_POST['postDaillyReportDowloadcsvallAttributs'];

	require 'dbconx.php';

	if($allAttributs == 'all'){
		$sq  = mysqli_query($con , "SELECT * FROM  tracking_student_downloads ORDER BY `id` DESC ");	
	}else{
		$sq  = mysqli_query($con , "SELECT * FROM  tracking_student_downloads WHERE student LIKE '%$allAttributs%' || moduleName LIKE '%$allAttributs%' ORDER BY `id` DESC ");	
	}

	if(mysqli_num_rows($sq)<1){
		echo "empty";exit;
	}

	$headers = mysqli_fetch_fields($sq );
	foreach($headers as $header){
		$head[] = $header->name;

	}
	
	$exportFile = 'xports/ExportFile-Download-portal.csv';

	$fp = fopen( $exportFile , 'w');
	if ($fp && $sq) {
	    
	    fputcsv($fp, array_values($head)); 
	    while ($row = mysqli_fetch_assoc($sq)) {
	        fputcsv($fp, array_values($row));
	    } 
	    fclose($fp);
	}
	echo  $exportFile;exit;

}

/********************************************************************/

if(isset($_POST['postDaillyReportcsv'])){

	$byBy 		= $_POST['postDaillyReportcsvbyBy'];
	$dateFilter = $_POST['postDaillyReportcsvdateFilter'];
	$allAttributs= $_POST['postDaillyReportcsvallAttributs'];

	require 'dbconx.php';

	if($allAttributs == 'all'){
		$sq  = mysqli_query($con , "SELECT * FROM  tracking_student_page ORDER BY `id` DESC ");	
	}else{
		$sq  = mysqli_query($con , "SELECT * FROM  tracking_student_page WHERE student LIKE '%$allAttributs%' || page LIKE '%$allAttributs%' ORDER BY `id` DESC ");	
	}

	if(mysqli_num_rows($sq)<1){
		echo "empty";exit;
	}

	$headers = mysqli_fetch_fields($sq );
	foreach($headers as $header){
		$head[] = $header->name;

	}
	
	$exportFile = 'xports/ExportFile-portal.csv';

	$fp = fopen( $exportFile , 'w');
	if ($fp && $sq) {
	    
	    fputcsv($fp, array_values($head)); 
	    while ($row = mysqli_fetch_assoc($sq)) {
	        fputcsv($fp, array_values($row));
	    } 
	    fclose($fp);
	}
	echo  $exportFile;exit;

}
/****************************************************************************/


if(isset($_POST['postFilterRequestDownloadHighCharts'])){
	
	$allAttributs= $_POST['postallAttributs'];

	require 'dbconx.php';
	// SELECT `moduleName`, count(*) as 'count'  FROM tracking_student_downloads GROUP BY `moduleName`
$sq  = mysqli_query($con , "SELECT `moduleName`, count(*) as 'count'  FROM tracking_student_downloads GROUP BY `moduleName` ");	
	
	
	$e = "";

	if(mysqli_num_rows($sq)<1){
		echo "empty";exit;
	}
	
	$data_modulName = $data_download_count  = $data_browserFireFox =$data_browserOther= $data_browserChrome = array();
	while($row = mysqli_fetch_assoc($sq)){		
		
		array_push( $data_modulName , array( $row['moduleName'] , (int) $row['count'] ) );
		
	}	

	$send = json_encode($data_modulName  );
	if($allAttributs == 'all'){

	?>	
	<style type="text/css">
      #container_33 {    
     margin:60px 80px;
	}
    </style>

	<div id="container_33" >
		<span id="hiddenData" style="display: none;"><?php echo $send ; ?></span>
		<div id="chart"></div>
	</div>

	<script type="text/javascript">
		
		var chart = new Highcharts.Chart({
			chart: {
				renderTo: 'chart'	
			},
			title: {
			        text: 'Downloads  Review'
			    },subtitle: {
        text: 'Source: Cut @ 2017'
    },
			yAxis: {
				title: {
					text: 'Downloads '
				}
			}, legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
			series: [
						{
							name: 'Downloads Count',
							data: <?php echo $send ;  ?>
						}
					],
					responsive: {
        rules: [{
            condition: {
                maxWidth: 700
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

		});
		
		
	</script>
	<?php

		}// end of if
		if($allAttributs == 'pie'){
			print $send;exit;
			?>
			
		<?php
		}// end of if

}




/****************************************************************************/
if(isset($_POST['postFilterRequestHighCharts'])){
	
	$allAttributs= $_POST['postallAttributs'];

	require 'dbconx.php';

	if($allAttributs == 'all'){
		$sq  = mysqli_query($con , "SELECT * FROM  tracking_student_page ORDER BY `id` DESC ");	
	}else{
		$sq  = mysqli_query($con , "SELECT * FROM  tracking_student_page WHERE student LIKE '%$allAttributs%' || page LIKE '%$allAttributs%'  ORDER BY `id` DESC ");	
	}

	if(mysqli_num_rows($sq)<1){
		echo "empty";exit;
	}
	$data_index =$data_results =$data_modules = $data_years = $data_profile = $data_Bursary = array();
	$sum_index  =$sum_results  =$sum_modules  = $sum_years = $sum_profile= $sum_busary =0;
	while($row = mysqli_fetch_assoc($sq)){
		$times = explode("|", $row['time_visit']);
		$lastTime = $times[0];
		if($row['page']== 'index' ){
			array_push($data_index ,$row['visit_count'] );
		}
		if($row['page']== 'results' ){
			array_push($data_results ,$row['visit_count'] );
		}
		if($row['page']== 'Modules' ){
			array_push($data_modules ,$row['visit_count'] );
		}
		if($row['page']== 'profile' ){
			array_push($data_profile ,$row['visit_count'] );
		}
		if($row['page']== 'Bursary' ){
			array_push($data_Bursary ,$row['visit_count'] );
		}
		array_push($data_years ,$row['date_visited'] );
		
	}
	$sum_index 		= array_sum($data_index);
	$sum_results 	= array_sum($data_results);
	$sum_modules 	= array_sum($data_modules);
	$sum_profile 	= array_sum($data_profile);
	$sum_busary 	= array_sum($data_Bursary);
	?>	
	<style type="text/css">
      #container_33 {
    /* background: red;
    border: 1px solid green;
    width: 500px;
    height: 200px; */
     margin:60px 80px;
}
    </style>

	<div id="container_33" >
		<span style="display: none;" id="hiddenData"><?php 
		echo json_encode( array(
			array("sum_index" 	, 	$sum_index),
			array("sum_results" ,	$sum_results),
			array("sum_modules" , 	$sum_modules),
			array("sum_profile" , 	$sum_profile),
			array("sum_busary" 	, 	$sum_busary)
		)) ; 
		?></span>
		<div id="chart"></div>
	</div>

	<script type="text/javascript">
		var chart = new Highcharts.Chart({
			chart: {
				renderTo: 'chart',	/*,           
            width: 700,
            height: 400*/
			},
			title: {
			        text: 'Dailly Modules Review'
			    },subtitle: {
        text: 'Source: Cut @ 2017'
    },
			yAxis: {
				title: {
					text: 'Visists '
				}
			}, legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
			series: [
						{
							name: 'Index Page Visists',
							data: [<?php echo join($data_index, ',') ?>]
						},
						{
							name: 'Modules Page Visists',
							data: [<?php echo join($data_modules, ',') ?>]
						},
						{
							name: 'Results Page Visists',
							data: [<?php echo join($data_results, ',') ?>]
						},
						{
							name: 'Profile Page Visists',
							data: [<?php echo join($data_profile, ',') ?>]
						},
						{
							name: 'Bursary Page Visists',
							data: [<?php echo join($data_Bursary, ',') ?>]
						}

					],
					responsive: {
        rules: [{
            condition: {
                maxWidth: 700
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

		});
	</script>
	<?php
}

/****************************************************************************/


if(isset($_POST['postFilter'])){

	$by 		= $_POST['postfilterBy'];
	

	require 'dbconx.php';	
	if($by == 'page'){	

		$sq  = mysqli_query($con , "SELECT *  FROM  tracking_student_page  ORDER BY `id` DESC ");

		if(mysqli_num_rows($sq)<1){
			echo "empty";exit;
		}
		?>
		<table class="table table-bordered">
			<thead>
				<tr>   
					<th>Total Instances</th>
                    <th>Activity</th>
                    
				</tr>
			</thead>
			<tbody  >
                              	<?php
		while($row = mysqli_fetch_assoc($sq)){		
			
			?>
			<tr>                                 
				<td><?php echo ucwords($row['visit_count']) ;?></td>
				<td><?php echo strtoupper($row['page']) ;?></td>			
				
			</tr>

			<?php
		}
		?>
	</tbody>
</table>
		<?php
		exit;
	}// end of if
	if($by == 'time_visit'){	
		
		$sq  = mysqli_query($con , "SELECT *  FROM  tracking_student_page  ORDER BY `id` DESC ");

		if(mysqli_num_rows($sq)<1){
			echo "empty";exit;
		}
		?>
		<table class="table table-bordered">
			<thead>
				<tr>   
					<th>Total Instances</th>
                    <th>Time</th>

                    
				</tr>
			</thead>
			<tbody  >
                              	<?php
		while($row = mysqli_fetch_assoc($sq)){	
		$times = explode("|", $row['time_visit']);
		$lastTime = $times[0];	
			
			?>
			<tr>                                 
				<td><?php echo ucwords($row['visit_count']) ;?></td>
				<td><?php echo strtoupper($lastTime) ;?></td>			
				
			</tr>

			<?php
		}
		?>
	</tbody>
</table>
		<?php
		exit;
	}// end of if
	if($by == 'date_visited'){	
		
		$sq  = mysqli_query($con , "SELECT *  FROM  tracking_student_page  ORDER BY `id` DESC ");

		if(mysqli_num_rows($sq)<1){
			echo "empty";exit;
		}
		?>
		<table class="table table-bordered">
			<thead>
				<tr>   
					<th>Total Instances</th>
                    <th>Date</th>                    
				</tr>
			</thead>
			<tbody  >
                              	<?php
		while($row = mysqli_fetch_assoc($sq)){		
			
			?>
			<tr>                                 
				<td><?php echo ucwords($row['visit_count']) ;?></td>
				<td><?php echo strtoupper($row['date_visited']) ;?></td>			
				
			</tr>

			<?php
		}
		?>
	</tbody>
</table>
		<?php
		exit;
	}// end of if
	if($by == 'act_std'){	
		
		$sq  = mysqli_query($con , "SELECT *  FROM  tracking_student_page  ORDER BY `id` DESC ");

		if(mysqli_num_rows($sq)<1){
			echo "empty";exit;
		}
		?>
		<table class="table table-bordered">
			<thead>
				<tr>   
					<th>Total Instances</th>
                    <th>Activity/Page</th>                    
                    <th>Student No.</th>                    
				</tr>
			</thead>
			<tbody  >
                              	<?php
		while($row = mysqli_fetch_assoc($sq)){		
			
			?>
			<tr>                                 
				<td><?php echo ucwords($row['visit_count']) ;?></td>
				<td><?php echo strtoupper($row['page']) ;?></td>			
				<td><?php echo strtoupper($row['student']) ;?></td>			
				
			</tr>

			<?php
		}
		?>
	</tbody>
</table>
		<?php
		exit;
	}// end of if

}

/****************************************************************************/

if(isset($_POST['postFilterRequest'])){

	$byBy 		= $_POST['postbyBy'];
	$dateFilter = $_POST['postdateFilter'];
	$allAttributs= $_POST['postallAttributs'];

	require 'dbconx.php';

	if($allAttributs == 'all'){
		$sq  = mysqli_query($con , "SELECT * FROM  tracking_student_page ORDER BY `id` DESC ");	
	}else{
		$sq  = mysqli_query($con , "SELECT * FROM  tracking_student_page WHERE student LIKE '%$allAttributs%' || page LIKE '%$allAttributs%'  ORDER BY `id` DESC ");	
	}

	if(mysqli_num_rows($sq)<1){
		echo "empty";exit;
	}
	while($row = mysqli_fetch_assoc($sq)){
		$times = explode("|", $row['time_visit']);
		$lastTime = $times[0];
		?>
		<tr>                                 
			<td><?php echo ucwords($row['page']) ;?></td>
			<td><?php echo strtoupper($row['student']) ;?></td>
			<td><?php echo $lastTime; ?></td>
			<td><?php echo $row['date_visited'] ;?></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['visit_count'] ;?></td>
			<td><?php echo $row['ip'] ;?></td>
		</tr>

		<?php
	}
	

}
/****************************************************************************/

if(isset($_POST['postFilterDownload'])){

	$byBy 		= $_POST['postbyBy'];
	$dateFilter = $_POST['postdateFilter'];
	$allAttributs= $_POST['postallAttributs'];

	require 'dbconx.php';

	if($allAttributs == 'all'){
		$sq  = mysqli_query($con , "SELECT * FROM  tracking_student_downloads ORDER BY `id` DESC ");	
	}else{
		$sq  = mysqli_query($con , "SELECT * FROM  tracking_student_downloads WHERE student LIKE '%$allAttributs%' || moduleName LIKE '%$allAttributs%'  ORDER BY `id` DESC ");	
	}

	if(mysqli_num_rows($sq)<1){
		echo "empty";exit;
	}
	while($row = mysqli_fetch_assoc($sq)){
		$times = explode("|", $row['time_download']);
		$lastTime = "";
		foreach($times as $time){
			$lastTime .= $time  . "<br>";
		}
		
		$browsers = explode("|", $row['browser']);
		$browser_list = "";
		foreach($browsers as $brow ){
				$browser_list .=  $brow . "<br>";
		}
		?>
		<tr>                                 
			<td><?php echo ucwords($row['moduleName']) ;?></td>
			<td><?php echo strtoupper($row['student']) ;?></td>
			<td><?php echo $lastTime; ?></td>
			<td><?php echo $row['date_download'] ;?></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['download_count'] ;?></td>
			<td><?php echo $browser_list ;?></td>
		</tr>

		<?php
	}
	

}


/****************************************************************************/



/****************************************************************************/




/****************************************************************************/




/****************************************************************************/

if(isset($_POST['postFilesUploadsLecturer'])){

	$WhichLecture = $_POST['postWhichLecture'];
	$ModuleCode = $_POST['postModuleCode'];
	require 'dbconx.php';
	if(mysqli_num_rows(mysqli_query($con, "SELECT lecturer 	FROM reading_materials WHERE lecturer='$WhichLecture'  "))>0){
		if(!empty($ModuleCode)){
			if(mysqli_num_rows(mysqli_query($con, $ModuleCode == 'all'?
				"SELECT module_code,lecturer 	FROM reading_materials WHERE lecturer='$WhichLecture'  "
				:
				"SELECT module_code,lecturer 	FROM reading_materials WHERE lecturer='$WhichLecture' AND module_code='$ModuleCode'  "
			))>0){
				// get files for this module
				$sq =   mysqli_query($con , $ModuleCode == 'all'? 
							"SELECT * FROM reading_materials WHERE  lecturer='$WhichLecture' "
							:
							"SELECT * FROM reading_materials WHERE  lecturer='$WhichLecture' AND module_code='$ModuleCode'"
							);
				while($wr = mysqli_fetch_assoc($sq)){
					?>

					<!-- <div class="form-group">
						<label class="control-label col-lg-2" for="inputSuccess">All </label>
						<div class="col-lg-10">
					
						</div>
					</div> -->
					<!-- <select >
					  	<option>1</option>
					  	<option>2</option>
					  	<option>3</option>
					  	<option>4</option>
					  	<option>5</option>
					  </select>  -->  
					<!-- <br><hr>                                -->

					<a class="list-group-item" href="javascript:;"> <?php echo $wr['doc_name_title'] . ' - '.$wr['module_code']; ?></a>


					<?php
				} exit;
			}else{
				echo "Modnuthing";exit;
			}
		}
	}else{
		echo "nuthing";exit;
	}
	
}


/****************************************************************************/


if(isset($_FILES['lecturerfile']) && isset($_POST['posTdocName']) && isset($_POST['posTprogramSelect']) && isset($_POST['posTuser']) ){

	$fileName = $_FILES["lecturerfile"]["name"]; // The file name
	$fileTmpLoc = $_FILES["lecturerfile"]["tmp_name"]; // File in the PHP tmp folder	
	$fileSize = $_FILES["lecturerfile"]["size"]; // File size in bytes
	$fileErrorMsg = $_FILES["lecturerfile"]["error"]; // 0 for false... and 1 for true
	$documentTitle = $_POST['posTdocName']; 
	$module = $_POST['posTprogramSelect'];
	$user = $_POST['posTuser'];
	require 'dbconx.php';
	if(mysqli_num_rows(mysqli_query($con , "SELECT * FROM reading_materials WHERE doc_name_title='$documentTitle' "))>0){
		echo "exists";exit;
	}

	if(move_uploaded_file($fileTmpLoc, "uploads/lecture/$fileName")){
		if(mysqli_query($con , "INSERT INTO reading_materials ( module_code , doc_name_title , fileName , lecturer ) VALUES ('$module','$documentTitle','$fileName','$user') ")){
			echo "Complete";exit;

		}else{
			echo "failed";exit;
		}	
		
	}else{
		echo "failed";exit;
	}
	
}





/****************************************************************************/
if(isset($_FILES['postAssigmentUploadFile'])){

$user       = $_POST['postUploaduser'];
$UserType   = $_POST['postUploadUserType'];
$modulName  = $_POST['postUploadmodulName'];
$date__		= date('Y-m-d');
$time_now 	= date("H:i:s"); 

$file 		= $_FILES['postAssigmentUploadFile']['name'];
$filetemp 	= $_FILES['postAssigmentUploadFile']['tmp_name'];


require 'dbconx.php';
if($UserType == 'student'){
if(mysqli_num_rows(mysqli_query($con , "SELECT * FROM student_file_uploads WHERE student ='$user' AND modulName = '$modulName' AND fileName= '$file'     "))>0){
	echo "exists";exit;
}else{
	if(!move_uploaded_file($filetemp , "student_ass/$file")){
	echo "failedupload";
	exit;
	}
	if(mysqli_query($con, "INSERT INTO student_file_uploads ( student, modulName, date_uploaded , fileName, time_uploaded ) VALUES ('$user','$modulName','$date__', '$file', '$time_now') ")){
		echo "saved";exit;
	}else{
		echo "failedsaved"; exit;
	}
}

}


}





/****************************************************************************/

if(isset($_POST['postfileDownload'])){
	$pageName 	= $_POST['postfileDownloadpageName'];
	$user 		= $_POST['postfileDownloaduser'];
	$UserType 	= $_POST['postfileDownloadUserType'];
	$moduleCode = $_POST['postfileDownloadmoduleCode'];
	$filename 	= $_POST['postfileDownloadfilename'];
	$browser 	= getBrowser()['name'];
	$date__		= date('Y-m-d');
	$time_now 	= date("H:i:s"); 

	require 'dbconx.php';
	if($UserType == 'student'){
		$qy = mysqli_query($con , "SELECT * FROM tracking_student_downloads WHERE student = '$user' AND moduleName='$moduleCode' AND fileName = '$filename' AND date_download = '$date__'  ");
		if(mysqli_num_rows($qy)>0)	{
			$row  = mysqli_fetch_assoc($qy);
			$times = $row ['time_download'] .$time_now. '|';
			$browser_ = $row['browser'] .$browser . '|';
			if(mysqli_query($con , "UPDATE tracking_student_downloads SET download_count  = download_count + '1' ,browser = '$browser_' , time_download = '$times'  WHERE moduleName='$moduleCode' AND fileName = '$filename' AND date_download = '$date__' ")){
				downloadFile("uploads/".$filename);
				echo  "updated";exit;
			}else{
				echo "failedUpdate";exit;
			}

		}else{
			$time_now .= '|';
			$browser  .= '|';
			if(mysqli_query($con  , "INSERT INTO tracking_student_downloads (student , pageName , date_download , time_download , fileName, download_count,moduleName, browser) VALUES ('$user','$pageName','$date__','$time_now','$filename','1','$moduleCode','$browser' ) ")){
				downloadFile("uploads/".$filename);
				echo "saved";exit;
			}else{
				echo "failedSave";exit;
			}
		}
	}


}

/****************************************************************************/

// echo date('l jS \of F Y h:i:s A');
// date("H:i:s");                         // 17:16:18

/****************************************************************************/
if(isset($_POST['postTrackPageVisit'])){

	$postpageName = $_POST['postpageName'];
	$postuser	  = $_POST['postuser'];
	$ip 		  = get_ip_address();
	$browser 	  = getBrowser()['name'];
	$date__		  = date('Y-m-d');
	$time_now 	  = date("H:i:s"); 
	require 'dbconx.php';
	if($_POST['postUserType'] == 'student'){
		// cheking if the user has visted this page on same day , so we update this page record on this day
		if(mysqli_num_rows(mysqli_query($con , "SELECT student ,date_visited , page FROM tracking_student_page WHERE student = '$postuser' AND date_visited = '$date__' AND page = '$postpageName'  "))>0){
			$row = mysqli_fetch_assoc(mysqli_query($con , "SELECT * FROM tracking_student_page WHERE student = '$postuser' AND date_visited = '$date__' AND page = '$postpageName'"));
			$times = $row ['time_visit'] .$time_now. '|';
			$browser_with = $row['browser'] . $browser . '|';			
				
			if(mysqli_query($con ,"UPDATE tracking_student_page SET visit_count = visit_count + '1' ,time_visit = '$times' , browser = '$browser_with'  WHERE student = '$postuser' AND date_visited = '$date__' AND page = '$postpageName'")){
				echo  "updated";exit;
			}else{
				echo "failedUpdate";exit;
			}
		}else{
			$time_now .= '|';
			$browser  .= '|';
			// this visitor has opened this page for the first time
			if(mysqli_query($con , "INSERT INTO tracking_student_page (student , page , date_visited, time_visit , ip , visit_count,browser) VALUES('$postuser','$postpageName','$date__','$time_now','$ip','1','$browser')")){
				echo "saved";exit;
			}else{
				echo "failedSave";exit;
			}
		}
	}
}
/****************************************************************************/

if(isset($_POST['postTrackPageVisitLecturer'])){
	$postpageName = $_POST['postpageNameLecturer'];
	$postuser	  = $_POST['postuserLecturer'];
	$ip 		  = get_ip_address();
	$browser 	  = getBrowser()['name'];
	$date__		  = date('Y-m-d');
	$time_now 	  = date("H:i:s"); 
	require 'dbconx.php';
	
		// cheking if the user has visted this page on same day , so we update this page record on this day
		if(mysqli_num_rows(mysqli_query($con , "SELECT lecturer ,date_visited , page FROM tracking_lecturer_page WHERE lecturer = '$postuser' AND date_visited = '$date__' AND page = '$postpageName'  "))>0){
			$row = mysqli_fetch_assoc(mysqli_query($con , "SELECT * FROM tracking_lecturer_page WHERE lecturer = '$postuser' AND date_visited = '$date__' AND page = '$postpageName'"));
			$times = $row ['time_visit'] .$time_now. '|';
			$browser_with = $row['browser'] . $browser . '|';			
				
			if(mysqli_query($con ,"UPDATE tracking_lecturer_page SET visit_count = visit_count + '1' ,time_visit = '$times' , browser = '$browser_with'  WHERE lecturer = '$postuser' AND date_visited = '$date__' AND page = '$postpageName'")){
				echo  "updated";exit;
			}else{
				echo "failedUpdate";exit;
			}
		}else{
			$time_now .= '|';
			$browser  .= '|';
			// this visitor has opened this page for the first time
			if(mysqli_query($con , "INSERT INTO tracking_lecturer_page (lecturer , page , date_visited, time_visit , ip , visit_count,browser) VALUES('$postuser','$postpageName','$date__','$time_now','$ip','1','$browser')")){
				echo "saved";exit;
			}else{
				echo "failedSave";exit;
			}
		}
	
}

/****************************************************************************/
if(isset($_POST['createAccount_student_Post'])){

	$user = stripslashes(trim($_POST['regnumber']));
	$conPassword = stripslashes(trim($_POST['conPassword']));
	$createAccountType = stripslashes(trim($_POST['userTypePost']));

	$firstname = stripslashes(trim($_POST['firstname']));
	$lastname = stripslashes(trim($_POST['lastname']));
	$stdprograms = stripslashes(trim($_POST['stdprograms']));
	$stdlevel = stripslashes(trim($_POST['stdlevel']));

	require 'dbconx.php';

	if($createAccountType == 'student'){
		if(mysqli_num_rows(mysqli_query($con , "SELECT * FROM users_students WHERE registration_number = '$user'   "))>0){
				echo "alreadyExist";exit;
			}

	}
	if($createAccountType == 'lecturer'){
		if(mysqli_num_rows(mysqli_query($con , "SELECT * FROM users_lectures WHERE registration_number = '$user'   "))>0){
			echo "alreadyExist";
			exit;

		}
	}
	if($createAccountType == 'admin'){
		if(mysqli_num_rows(mysqli_query($con , "SELECT * FROM users WHERE registration_number = '$user'   "))>0){
			echo "alreadyExist";
			exit;

		}
	}
	
	$date__ = date('Y-m-d');
	if($createAccountType == 'student'){
		$sql = mysqli_query($con , "INSERT INTO users_students (name , surname , registration_number , password ,  date_created ,level , program) VALUES
			('$firstname','$lastname','$user','$conPassword','$date__','$stdlevel','$stdprograms')   ");
	}
	if($createAccountType == 'lecturer'){
		$sql = mysqli_query($con , "INSERT INTO users_lectures (name , surname , registration_number , password ,  date_created  , program) VALUES
			('$firstname','$lastname','$user','$conPassword','$date__','$stdprograms')   ");
	}
	if($createAccountType == 'admin'){
		$sql = mysqli_query($con , "INSERT INTO users (registration_number ,name , surname ,password,userType , date_created) VALUES ('$user','$firstname','$lastname','$conPassword','$createAccountType' , '$date__' )   ");
	}

	echo $sql ? "saved":"error";exit();



}
/****************************************************************************/




/****************************************************************************/


/****************************************************************************/
if(isset($_POST['loginAccount_student_Post'])){

	$user = stripslashes(trim($_POST['loginregnumber']));
	$password = stripslashes(trim($_POST['loginpassword']));
	$createAccountType = stripslashes(trim($_POST['loginuserTypePost']));

	
	
	if($createAccountType == "student"){
		$q = "SELECT * FROM users_students WHERE registration_number = '$user'  AND password = '$password'  " ;
	}
	if($createAccountType == "lecturer"){
		$q = "SELECT * FROM users_lectures WHERE registration_number = '$user'  AND password = '$password'  " ;
	}
	if($createAccountType == "admin"){
		$q = "SELECT * FROM users WHERE registration_number = '$user'  AND password = '$password'  " ;
	}
	require 'dbconx.php';
	if(mysqli_num_rows(mysqli_query($con , $q))>0){
		session_start();
		$_SESSION['user'] = $user;
		$_SESSION['userType'] = $createAccountType;
		echo 'found';
		exit();
	}else{
		$ip = get_ip_address();
		$date__ = date('Y-m-d');
		#$s = mysqli_query($con , "SELECT * FROM attemp_login WHERE ip = '$ip' ");
		mysqli_query($con , "INSERT INTO attemp_login (ip , user_type , user , password , date_visited , attempt_count ) VALUES ('$ip', '$createAccountType', '$user', '$password', '$date__' , '1')   ");

		echo "unfound";
		exit();
	}


}

/****************************************************************************/


/****************************************************************************/
function get_ip_address(){
		//NB. or PLEASE NOTE
	$http_client_ip=$_SERVER['SERVER_ADDR'];
	$http_x_forwad_for=$_SERVER['HTTP_USER_AGENT'];
		$remote_address=$_SERVER['REMOTE_ADDR'];///gets the ip address
	    //$http_x_forwad_for,$remote_address could be empty
	    #so they may be set empty before they are passed to the function
		if (!empty($http_client_ip)) {
			return $http_client_ip;
	        #store in database

		}else if(!empty($http_x_forwad_for)){
			return $http_x_forwad_for;
	        #store in database
		}else{
			return $remote_address;
	        #store in database
		}

	}//endof get_ip_address

/****************************************************************************/

function downloadFile($filepath){
	if (!is_file($filepath)) 
		   {
			 echo("404 File not found!"); // file not found to download
			 exit();
		   }
		
		   
			$len = filesize($filepath); // get size of file
			$filename = basename($filepath); // get name of file only
			$file_extension = strtolower(pathinfo($filename,PATHINFO_EXTENSION));
			//Set the Content-Type to the appropriate setting for the file
			switch( $file_extension )
				 {
					case "pdf": $ctype="application/pdf"; break;
					case "exe": $ctype="application/octet-stream"; break;
					case "zip": $ctype="application/zip"; break;
					case "doc": $ctype="application/msword"; break;
					case "xls": $ctype="application/vnd.ms-excel"; break;
					case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
					case "gif": $ctype="image/gif"; break;
					case "csv" : $ctype="text/csv";break;
					case "png": $ctype="image/png"; break;
					case "jpeg":
					case "jpg": $ctype="image/jpg"; break; 
					default: $ctype="application/force-download";
				}
				ob_clean();
				//Begin writing headers
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: public"); 
				header("Content-Description: File Transfer");
				//Use the switch-generated Content-Type
				header("Content-Type: $ctype");
				//Force the download
				$header="Content-Disposition: attachment; filename=".$filename.";";
				header($header );
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".$len);
				@readfile($filepath);
				
}

/****************************************************************************/


function getBrowser(){ 
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
    
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
}

/**************************************************************************/
?>