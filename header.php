 <?php 

session_start();
if(!isset($_SESSION['user'])){
    header("location:login.php");
}
$userType = $_SESSION['userType'];
 ?>

 <header class="header dark-bg">
            <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
            </div>

            <!--logo start-->
            <a href="<?php echo $userType=='admin'?'admin_index.php':'index.php'  ?>" style="text-transform: capitalize;" class="logo">E-Portal Tracking System  <span class="lite"><?php echo ' '. $_SESSION['userType']; ?></span></a>

            <label style="display: none;" id="GlobalUserReadFrom"><?php echo $_SESSION['user'] ; ?></label>
            <label style="display: none;" id="GlobalPageReadFrom"><?php echo $PAGE; ?></label>
            <label style="display: none;" id="GlobalUserTypeReadFrom"><?php echo $_SESSION['userType']; ?></label>
            <?php if( $_SESSION['userType'] == 'admin' ){ ?>
            <center> <label class="logo" style="display: none;" >Main Adminstrator Scene.</label> </center>
            <?php } ?>

            <!--logo end-->

            

            <div class="top-nav notification-row">                
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">
                    
                    
                    <!-- task notificatoin end -->

                  
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                           
                            <span class="username" style="text-transform: capitalize;">User : <?php echo $_SESSION['user'] ; ?></span >
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="logout.php">Log out</a>
                            </li>
                            
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>   