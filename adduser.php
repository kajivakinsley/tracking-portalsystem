<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">
  <?php $PAGE = 'Add User'; ?>
  <title> Admin  page </title>

  <!-- Bootstrap CSS -->    
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />    
  <!-- Custom styles -->
  <link rel="stylesheet" href="css/fullcalendar.css">
  <link href="css/widgets.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />
  
  <link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">

  <link rel="stylesheet" href="css/bootstrap-dialog.min.css">

</head>


<body>
  <!-- container section start -->
  <section id="container" class="">



    <!--header end-->
    <?php  include 'header.php' ; ?>   
    <!--sidebar start-->
    <?php  include 'sidebar.php' ; ?>   
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">            
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">

            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="admin_index.php">Home</a></li>

            </ol>
          </div>
        </div>

        <!-- Today status end -->              
        
        <div class="row">

          <div class="col-lg-6">
            <section class="panel">
              <header class="panel-heading">
               Add a new User
             </header>
             <div class="panel-body">
              <form class="form-horizontal" id="fomFileUpload" role="form" onsubmit="return false;">
                <div class="form-group">
                  <label for="inputEmail1" class="col-lg-2 control-label">Select User Type</label>
                  <div class="col-lg-10">
                   <select id="createAccountType" class="selectpicker form-control" data-live-search="true" onchange="iconChangeAdmin();">
                    <option value= "null">Select User</option>
                    <option value= "student">Student</option>
                    <option value= "lecturer">Lecturer</option>
                    <option value= "admin">Administrator</option>

                  </select>

                </div>
              </div>
              <div class="form-group" id="stdlevelAdmin" style="display: none;">
                <label for="inputEmail1" class="col-lg-2 control-label">Select Level</label>
                <div class="col-lg-10">
                  <select id="stdlevel" class="selectpicker form-control" data-live-search="true" >
                    <option value= "null">Select Level</option>
                  <option value="1.1">Level 1.1</option>
                  <option  value="1.2">Level 1.2</option>
                  <option value="2.1">Level 2.1</option>
                  <option  value="2.2">Level 2.2</option>
                  <option  value="3.1">Level 3.1</option>
                  <option  value="3.2">Level 3.2</option>
                  <option  value="4.1">Level 4.1</option>
                  <option  value="4.2">Level 4.2</option>

                  </select>

                </div>
              </div>
              <div class="form-group" style="display: none;" id="stdprogramsAdmin">
                <label for="inputEmail1" class="col-lg-2 control-label">Program</label>
                <div class="col-lg-10">
                   <select id="stdprograms" class="selectpicker form-control" data-live-search="true" >
                   
                 <option value= "null">Select Program</option>
              <?php 
              require 'dbconx.php';              
              $sq= "SELECT DISTINCT `dept`,`gname` FROM `programs` ";
              $q = mysqli_query($con , $sq);
              while($rw = mysqli_fetch_assoc($q)){
                ?>
                <option value="<?php echo $rw['gname']; ?>" ><?php echo $rw['gname'] .' - '.$rw['dept']; ?></option>
                <?php
              } ?>

                  </select>

                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail1" class="col-lg-2 control-label">First Name</label>
                <div class="col-lg-10">
                  <input type="text" name="firstname" id="firstname" class="form-control input-lg" placeholder="First Name"  /> 

                </div>
              </div>
               <div class="form-group">
                <label for="inputEmail1" class="col-lg-2 control-label">Last Name</label>
                <div class="col-lg-10">
                  <input type="text" name="lastname" id="lastname" class="form-control input-lg" placeholder="Last Name"  /> 

                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail1" class="col-lg-2 control-label">Registration Number</label>
                <div class="col-lg-10">
                  <input class="form-control" type="text" id="regNumber" placeholder="Registration Number..." autofocus > 

                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail1" class="col-lg-2 control-label">Password</label>
                <div class="col-lg-10">
                   <input class="form-control" type="password" id="password" placeholder="Password">

                </div>
              </div>
            
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                   <a href="#" class="btn  btn btn-primary" id="addUserAdmin" type="submit"> Create Account </a> 
                </div>
              </div>
            </form>
          </div>
        </section>
      </div>
    </div>



    <!-- statics end -->





  </section>

</section>
<!--main content end-->
</section>
<!-- container section start -->

<!-- javascripts -->
<script src="js/jquery.js"></script>
<script src="js/jquery-ui-1.10.4.min.js"></script>
<script src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<!-- bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.nicescroll.js" type="text/javascript"></script>   
<!--custome script for all page-->
<script src="js/scripts.js"></script>
<!-- custom script for this page-->

<script src="js/jquery.autosize.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/gdp-data.js"></script>  
<script src="js/morris.min.js"></script>

<script src="js/jquery.slimscroll.min.js"></script>
<!-- cutsome use -->
<script type="text/javascript" src="js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" src="js/loadingoverlay.min.js"></script>
<script type="text/javascript" src="js/loadingoverlay_progress.min.js"></script>



<script src="js/customeQuery.js"></script> 
<script type="text/javascript">


</script>

</body>
</html>
