<optgroup label="School of Agricultural Sciences & Technology"> 
  <option value="Agricultural Engineering">Agricultural Engineering</option>
  <option value="Animal Production & Technology">  Animal Production & Technology</option>
  <option value="Environmental Science"> Environmental Science and Technology </option>
  <option value="Environmental Health">  Environmental Health </option>
  <option value="Irrigation Engineering">  Irrigation Engineering  </option>
  <option value="Biotechnology"> Biotechnology (BSCBIO) </option>
  <option value="Food Science and Technology">Food Science and Technology</option>
  <option value="Crop Science and Technology"> Crop Science and Technology   </option>
</optgroup>

<optgroup label="School of Art & Design"> 
  <option value="Creative Art And Design">Creative Art And Design</option>
  <option value="Clothing and Textile Technology"> Clothing and Textile Technology</option>

</optgroup>

<optgroup label="School of Entrepreneurship & Business Sciences"> 
  <option value="Accountancy">Accountancy</option>
  <option value="Business Management And Entreprenuership">Business Management And Entreprenuership</option>
  <option value="Marketing"> Marketing </option>
  <option value="Consumer Science">Consumer Science</option>
  <option value="Supply Chain Management">Supply Chain Management</option>

</optgroup>

<optgroup label="School of Engineering Science & Technology">                                                            
   <option value="Fuels and Energy Engineering">Fuels and Energy Engineering</option>
   <option value="Mechatronics Engineering">Mechatronics Engineering</option>
   <option value="Production Engineering">Production Engineering</option>
   <option value="Environmental Engineering">Environmental Engineering</option>
   <option value="ICT">ICT</option>
   
</optgroup>

<optgroup label="Graduate Business School"> 
   <option value="Applied Entrepreneurship">Applied Entrepreneurship </option>
   <option value="Strategic Management"> Strategic Management</option>
   <!-- <option value="Supply Chain Management">Supply Chain Management  </option> -->

</optgroup>

<optgroup label="School of Hospitality & Tourism"> 
   <option value="Hospitality and Tourism">Hospitality and Tourism </option>
   <option value="Travel and Recreation"> Travel and Recreation </option>

</optgroup>

<optgroup label="School of Natural Sciences & Mathematics"> 
 <option value="Biology"> Biology </option>
 <option value="Physics">Physics  </option>
 <option value="Mathematics"> Mathematics </option>
 <option value="Chemistry">Chemistry </option>
 <option value="Statistics"> Statistics </option>

</optgroup>