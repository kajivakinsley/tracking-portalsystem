<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">
  <?php $PAGE = 'Chat'; ?>
  <title> Chat | Portal page </title>
  

  <!-- Bootstrap CSS -->    
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />    
  <!-- Custom styles -->
  <link rel="stylesheet" href="css/fullcalendar.css">
  <link href="css/widgets.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />

  <link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">

  <link rel="stylesheet" href="css/bootstrap-dialog.min.css">

</head>

<body>
  <!-- container section start -->
  <section id="container" class="">



    <!--header end-->
    <?php  include 'header.php' ; ?>   
    <!--sidebar start-->
    <?php  include 'sidebar.php' ; ?> 
    <?php 
        if( $_SESSION['userType'] != 'lecturer' || $_SESSION['userType'] != 'student'){
         // header("location: logout.php");
          //exit;
        }

   ?>  
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">            
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">

           <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
            <li><i class="fa fa-laptop"></i><a href="chat.php">Chat</a></li>						  	
          </ol>
        </div>
      </div>

      <!-- Today status end -->              

      <div class="row">


        <div class="col-lg-12">
          <?php 
          require 'dbconx.php';

         
            $row = mysqli_fetch_assoc(mysqli_query($con , 
              $_SESSION['userType'] == 'student' ?  "SELECT * FROM users_students WHERE registration_number = '$_SESSION[user]' "
              :
               "SELECT * FROM users_lectures WHERE registration_number = '$_SESSION[user]' "
            ));
            ?>
            <section class="panel">
              <header class="panel-heading">
                Basic Infomation
              </header>
              <ul class="list-group">
                <li class="list-group-item">
                 <strong> User Name: <u><?php echo $row['name'] . ' ' . $row['surname'];  ?></u> </strong>


               </li>
               <li class="list-group-item">User Registred on date :<?php echo $row['date_created'] ;?></li>
               <li>  <form class="form-horizontal "  onsubmit="return false;">                                
                                  <div class="form-group">
                                    <div id="container11" style="margin: 20px 0;">
                                      
                                    </div>
                                     <label class="control-label col-lg-2" for="inputSuccess">Select Department</label> 
                                      <div class="col-lg-10">
                                         <div class="row">
                                             <div class="col-lg-2">
                                                
                                                 <select class="form-control">
                                                   <option value="Bsit">ICT</option>
                                                   <option value="BSBIO">BIOLOGY</option>
                                                   <option value="BSCHM">CHEMISTRY</option>
                                                    <option value="BSCSC">SUPPLY CHAIN</option>
                                                     <option value="BSBE">ENTREPRENEURSHIP</option>
                                                 </select>
                                             </div>
                                             <label class="control-label col-lg-2" for="inputSuccess">Select Lecturer</label> 
                                             <div class="col-lg-3">
                                                <select class="form-control">
                                                   <option value="Lecturer">Mr or Mrs 1</option>
                                                   <option value="Lecturer">Mr or Mrs 2</option>
                                                   <option value="Lecturer">Mr or Mrs 3</option>
                                                   <option value="Lecturer">Mr or Mrs 4</option>
                                                   <option value="Lecturer">Mr or Mrs 5</option>
                                                  
                                                 </select>
                                             </div>
                                             

                                         </div>
                                     
                                     </div> 
                                  </div>
                                  
                              </form>
                            </li>
                            <li> 
                              ....
                            </li>


             </ul>
           </section>


           <?php
         
         ?>
         

       </div>
       <div class="col-md-4 portlets">
        <!-- Widget -->
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="pull-left">Chat Messages</div>
            <div class="widget-icons pull-right">
             <!--  <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>  -->
              <a href="javascript:void(0)" class="wclose"><i class="fa fa-times"></i></a>
            </div>  
            <div class="clearfix"></div>
          </div>

          <div class="panel-body">
            <!-- Widget content -->
            <div class="padd sscroll">
              <?php if($_SESSION['userType'] == 'student'){ ?>
              <!-- start of chats  -->
              <ul class="chats">
                <!-- Chat by other. Use the class "by-other". -->
                <li class="by-other">
                  <!-- Use the class "pull-right" in avatar -->
                  <div class="avatar pull-right">
                  <img style="width: 50px; height: 60px;" src="img/student-icon.png" alt=""/>
                  </div>

                  <div class="chat-content">
                    <!-- In the chat meta, first include "time" then "name" -->
                    <div class="chat-meta">3 mins ago <span class="pull-right"><?php echo $row['name'] . ' ' . $row['surname'];  ?></span></div>
                   Good morning how are you ?
                    <div class="clearfix"></div>
                  </div>
                </li> 

                <!-- Chat by us. Use the class "by-me". -->
                <li class="by-me">
                  <!-- Use the class "pull-left" in avatar -->
                  <div class="avatar pull-left">
                    <img style="width: 50px; height: 60px;" src="img/admin-icon.png" alt=""/>
                  </div>

                  <div class="chat-content">
                    <!-- In meta area, first include "name" and then "time" -->
                    <div class="chat-meta">Lecturer <span class="pull-right">5 mins ago</span></div>
                   Hi ...
                    <div class="clearfix"></div>
                  </div>
                </li> 
                 <!-- Chat by other. Use the class "by-other". -->
                <li class="by-other">
                  <!-- Use the class "pull-right" in avatar -->
                  <div class="avatar pull-right">
                  <img style="width: 50px; height: 60px;" src="img/student-icon.png" alt=""/>
                  </div>

                  <div class="chat-content">
                    <!-- In the chat meta, first include "time" then "name" -->
                    <div class="chat-meta">9 mins ago <span class="pull-right"><?php echo $row['name'] . ' ' . $row['surname'];  ?></span></div>
                  When are going to meet today  and also give us a venue
                    <div class="clearfix"></div>
                  </div>
                </li> 

                <!-- Chat by us. Use the class "by-me". -->
                <li class="by-me">
                  <!-- Use the class "pull-left" in avatar -->
                  <div class="avatar pull-left">
                    <img style="width: 50px; height: 60px;" src="img/admin-icon.png" alt=""/>
                  </div>

                  <div class="chat-content">
                    <!-- In meta area, first include "name" and then "time" -->
                    <div class="chat-meta">Lecturer <span class="pull-right">13 mins ago</span></div>
                  Well i will tell you by mid day ,  maybe say  12.00
                    <div class="clearfix"></div>
                  </div>
                </li> 
                                                                                                

              </ul>
              <!-- end of chats  -->
              <?php } if($_SESSION['userType'] == 'lecturer'){ ?>
              <!-- start of chats  -->
              <ul class="chats">
                <!-- Chat by other. Use the class "by-other". -->
                <li class="by-other">
                  <!-- Use the class "pull-right" in avatar -->
                  <div class="avatar pull-right">
                  <img style="width: 50px; height: 60px;" src="img/student-icon.png" alt=""/>
                  </div>

                  <div class="chat-content">
                    <!-- In the chat meta, first include "time" then "name" -->
                    <div class="chat-meta">3 mins ago <span class="pull-right"><?php echo $row['name'] . ' ' . $row['surname'];  ?></span></div>
                   Good morning how are you ?
                    <div class="clearfix"></div>
                  </div>
                </li> 

                <!-- Chat by us. Use the class "by-me". -->
                <li class="by-me">
                  <!-- Use the class "pull-left" in avatar -->
                  <div class="avatar pull-left">
                    <img style="width: 50px; height: 60px;" src="img/admin-icon.png" alt=""/>
                  </div>

                  <div class="chat-content">
                    <!-- In meta area, first include "name" and then "time" -->
                    <div class="chat-meta"><?php echo $row['name'] . ' ' . $row['surname'];  ?> <span class="pull-right">5 mins ago</span></div>
                   Hi ...
                    <div class="clearfix"></div>
                  </div>
                </li> 
                 <!-- Chat by other. Use the class "by-other". -->
                <li class="by-other">
                  <!-- Use the class "pull-right" in avatar -->
                  <div class="avatar pull-right">
                  <img style="width: 50px; height: 60px;" src="img/student-icon.png" alt=""/>
                  </div>

                  <div class="chat-content">
                    <!-- In the chat meta, first include "time" then "name" -->
                    <div class="chat-meta">9 mins ago <span class="pull-right"> Student </span></div>
                  When are going to meet today  and also give us a venue
                    <div class="clearfix"></div>
                  </div>
                </li> 

                <!-- Chat by us. Use the class "by-me". -->
                <li class="by-me">
                  <!-- Use the class "pull-left" in avatar -->
                  <div class="avatar pull-left">
                    <img style="width: 50px; height: 60px;" src="img/admin-icon.png" alt=""/>
                  </div>

                  <div class="chat-content">
                    <!-- In meta area, first include "name" and then "time" -->
                    <div class="chat-meta">Lecturer <span class="pull-right">13 mins ago</span></div>
                  Well i will tell you by mid day ,  maybe say  12.00
                    <div class="clearfix"></div>
                  </div>
                </li> 
                                                                                                

              </ul>
              <!-- end of chats  -->


              <?php } ?>

            </div>
            <!-- Widget footer -->
            <div class="widget-foot">

              <form class="form-inline" onsubmit="return false;">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Type your message here...">
                </div>
                <button type="submit" class="btn btn-info">Send</button>
              </form>


            </div>
          </div>


        </div> 
      </div>


    </div>



    <!-- statics end -->





  </section>

</section>
<!--main content end-->
</section>
<!-- container section start -->

<!-- javascripts -->
<script src="js/jquery.js"></script>
<script src="js/jquery-ui-1.10.4.min.js"></script>
<script src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
<!-- bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.nicescroll.js" type="text/javascript"></script>   
<!--custome script for all page-->
<script src="js/scripts.js"></script>
<!-- custom script for this page-->

<script src="js/jquery.autosize.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/gdp-data.js"></script>	
<script src="js/morris.min.js"></script>

<script src="js/jquery.slimscroll.min.js"></script>
<!-- cutsome use -->
<script type="text/javascript" src="js/bootstrap-dialog.min.js"></script>
<script type="text/javascript" src="js/loadingoverlay.min.js"></script>
<script type="text/javascript" src="js/loadingoverlay_progress.min.js"></script>
<script src="js/customeQuery.js"></script> 
<script type="text/javascript">
  $(function() {
    <?php if($_SESSION['userType'] == 'student') {?>
      pagevisitStudent();
      <?php }else{?>

        pagevisitLecturer();
        <?php }?>

      });
    </script>

  </body>
  </html>
