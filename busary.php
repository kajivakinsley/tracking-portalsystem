<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">
<?php $PAGE = 'Bursary'; ?>
    <title> Home | Portal page </title>

    <!-- Bootstrap CSS -->    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="css/elegant-icons-style.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />    
    <!-- Custom styles -->
	<link rel="stylesheet" href="css/fullcalendar.css">
	<link href="css/widgets.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
	
	<link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap-dialog.min.css">
    
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
     
      
    
      <!--header end-->
 <?php  include 'header.php' ; ?>   
      <!--sidebar start-->
    <?php  include 'sidebar.php' ; ?>   
      <!--sidebar end-->
      
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">            
              <!--overview start-->
			  <div class="row">
				<div class="col-lg-12">
					
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
						<li><i class="fa fa-laptop"></i><a href="busary.php">My Bursay</a></li>						  	
					</ol>
				</div>
			</div>
		  
		  <!-- Today status end -->              
				
			<div class="row">
               	
				
				<div class="col-lg-12">
                    <?php 
                    require 'dbconx.php';
                  
                    if($_SESSION['userType'] == 'student'){
                    $row = mysqli_fetch_assoc(mysqli_query($con , "SELECT * FROM users_students WHERE registration_number = '$_SESSION[user]' "));
                    ?>
                    <section class="panel">
                          <header class="panel-heading">
                              Basic Infomation
                          </header>
                          <ul class="list-group">
                              <li class="list-group-item">
                               <strong> Student Name: <u><?php echo $row['name'] . ' ' . $row['surname'];  ?></u> </strong>

                                    
                                </li>
                              <li class="list-group-item">Student Registred on date :<?php echo $row['date_created'] ;?></li>
                             
                             
                          </ul>
                      </section>


                    <?php
                  }                 

                    ?>
                      
                  </div>
				
				
              </div>
               <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Reports 
                          </header>
                          <div id="content" class="table-responsive">
                            <table id="tableID" class="table">
                              <thead>
                                <tr>                                  
                                  <th>Date</th>
                                  <th>Description</th>
                                  <th>Credit</th>                                 
                                  <th>Debit</th>
                                
                                </tr>
                              </thead>
                              <tbody id="showBusryResults" >
                                <?php  
                                for($v = 1 ; $v < 20 ; $v++){
                                ?>
                                  <tr>
                                    <td><?php $f = '+'.$v; echo  date('Y-m-d', strtotime($f.' year')) ; ?></td>
                                    <td> Res Double Room March  </td>
                                    <td> --  </td>
                                    <td><?php echo $v * 10 * rand(30 , 200) ?> </td>
                                    
                                  </tr>
                                <?php } ?>

                               
                              </tbody>
                            </table>
                          </div>

                      </section>
                  </div>
              </div>


                    
                   
                <!-- statics end -->
              
            
				


          </section>
         
      </section>
      <!--main content end-->
  </section>
  <!-- container section start -->

    <!-- javascripts -->
    <script src="js/jquery.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <script src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <!-- bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>   
    <!--custome script for all page-->
    <script src="js/scripts.js"></script>
    <!-- custom script for this page-->
    
	<script src="js/jquery.autosize.min.js"></script>
	<script src="js/jquery.placeholder.min.js"></script>
	<script src="js/gdp-data.js"></script>	
	<script src="js/morris.min.js"></script>
	
	<script src="js/jquery.slimscroll.min.js"></script>
 <!-- cutsome use -->
 <script type="text/javascript" src="js/bootstrap-dialog.min.js"></script>
   <script type="text/javascript" src="js/loadingoverlay.min.js"></script>
   <script type="text/javascript" src="js/loadingoverlay_progress.min.js"></script>
   <script src="js/customeQuery.js"></script> 
   <script type="text/javascript">
  $(function() {
    <?php if($_SESSION['userType'] == 'student') {?>
  pagevisitStudent();
  <?php }else{?>

pagevisitLecturer();
    <?php }?>

});
</script>

  </body>
</html>
